// Amazon FPGA Hardware Development Kit
//
// Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
//
// Licensed under the Amazon Software License (the "License"). You may not use
// this file except in compliance with the License. A copy of the License is
// located at
//
//    http://aws.amazon.com/asl/
//
// or in the "license" file accompanying this file. This file is distributed on
// an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or
// implied. See the License for the specific language governing permissions and
// limitations under the License.

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

// Vivado does not support svGetScopeFromName
#ifdef INCLUDE_DPI_CALLS
#ifndef VIVADO_SIM
#include "svdpi.h"
#endif
#endif

#include "sh_dpi_tasks.h"
#include "testhelp.h"

#define HELLO_WORLD_REG_ADDR UINT64_C(0x00)


/*void test_main(uint32_t *exit_code) {*/

/*// Vivado does not support svGetScopeFromName*/
/*#ifdef INCLUDE_DPI_CALLS*/
/*#ifndef VIVADO_SIM*/
  /*svScope scope;*/
/*#endif*/
/*#endif*/

  /*uint32_t rdata;*/

/*// Vivado does not support svGetScopeFromName*/
/*#ifdef INCLUDE_DPI_CALLS*/
/*#ifndef VIVADO_SIM*/
  /*scope = svGetScopeFromName("tb");*/
  /*svSetScope(scope);*/
/*#endif*/
/*#endif*/

  /*log_printf("Writing 0xDEAD_BEEF to address 0x%x", HELLO_WORLD_REG_ADDR);*/
  /*cl_poke(HELLO_WORLD_REG_ADDR, 0xDEADBEEF);*/
  /*cl_peek(HELLO_WORLD_REG_ADDR, &rdata);*/

  /*log_printf("Reading 0x%x from address 0x%x", rdata, HELLO_WORLD_REG_ADDR);*/

  /*if (rdata == 0xEFBEADDE) {*/
    /*log_printf("Test PASSED");*/
  /*} else {*/
    /*log_printf("Test FAILED");*/
  /*}*/

  /**exit_code = 0;*/
/*}*/

/**
 * This is the Partitioner test.
 * This test only does one partition on key=startpos for every column
 **/

void test_main(uint32_t *exit_code) {

   // Addresses to be used throughout
   uint64_t keyAddr =           0x0;
   uint64_t column1Addr =       0x2000;
   uint64_t column2Addr =       0xA000;
   uint64_t column3Addr =       0x18000;
   // May need to increase these to fit larger data
   uint64_t outputKeyAddr =     0x26000;
   uint64_t outputColumn1Addr = 0x34000;
   uint64_t outputColumn2Addr = 0x42000;
   uint64_t outputColumn3Addr = 0x4A000;

   uint64_t input_addrs[] = {keyAddr, column1Addr, column2Addr, column3Addr};

   // Arrays to hold output data
   uint64_t outputKey[NUM_ELEMENTS];
   uint32_t outputColumn1[NUM_ELEMENTS];
   uint32_t outputColumn2[NUM_ELEMENTS];
   uint32_t outputColumn3[NUM_ELEMENTS];
   uint8_t outputColumn7[NUM_ELEMENTS];
   // These have to be malloc'd bc they are too big to fit on the stack
   uint64_t* outputColumn4 = (uint64_t*) malloc(4 * NUM_ELEMENTS * sizeof(uint64_t));
   uint64_t* outputColumn5 = (uint64_t*) malloc(4 * NUM_ELEMENTS * sizeof(uint64_t));
   uint64_t* outputColumn6 = (uint64_t*) malloc(4 * NUM_ELEMENTS * sizeof(uint64_t));

   // Constants that may need changing between test iterations
   int num_col = 3;
   int num_div = 5; // Note: partitioner can only accept 8 splitter values. see Howie for more details

   // Data needs to be moved from input file to FPGA
   printf("Transferring Data to FPGA...\n");
   transfer_chunk_to_fpga(startpos, keyAddr, 64);
   //transfer_chunk_to_fpga((uint32_t*)matepos, column1Addr, 32);
   //transfer_chunk_to_fpga((uint32_t*)chromkey, column2Addr, 32);
   //transfer_chunk_to_fpga((uint32_t*)matechromkey, column3Addr, 32);

   //// Set up & run the Partitioner
   //int sizes1[4] = {SIZE_32, SIZE_32, SIZE_32, SIZE_32};
   //partitionRun(num_col, num_div, sizes1,
		   //keyAddr, column1Addr, column2Addr, column3Addr,
			  //outputKeyAddr, outputColumn1Addr, outputColumn2Addr, outputColumn3Addr);

   //// Retrieve data from the FPGA
   //printf("Getting Data from FPGA...\n");
   //get_chunk_from_fpga((uint32_t*) outputKey, outputKeyAddr, 32);
   //get_chunk_from_fpga((uint32_t*) outputColumn1, outputColumn1Addr, 32);
   //get_chunk_from_fpga((uint32_t*) outputColumn2, outputColumn2Addr, 32);
   //get_chunk_from_fpga((uint32_t*) outputColumn3, outputColumn3Addr, 32);
   //flush(handle);

   //// Check the output for correctness
   //printf("Checking Output for Correctness...\n");
   //for (int i = 0; i < num_div + 1; i++) {
       //int count = partition_get_count(0, i);
       //COMPARE(i, counts[i], count, "partition counts");
   //}
   //for (int i = 0; i < NUM_ELEMENTS; i++) {
       //COMPARE(i, e_startpos[i], outputKey[i], "key");
       //COMPARE(i, e_matepos[i], outputColumn1[i], "col1");
       //COMPARE(i, e_chromkey[i], outputColumn2[i], "col2");
       //COMPARE(i, e_matechromkey[i], outputColumn3[i], "col3");
   //}


   //// Second Iteration: key=startpos, columns=endpos,mapq,grpname
   //printf("Transferring Data to FPGA...\n");
   //transfer_chunk_to_fpga((uint32_t*)endpos, column1Addr, 32);
   //transfer_chunk_to_fpga((uint32_t*)mapq, column2Addr, 32);
   //transfer_chunk_to_fpga((uint32_t*)grpname, column3Addr, 128);

   //int sizes2[4] = {SIZE_32, SIZE_32, SIZE_32, SIZE_128};
   //partitionRun(num_col, num_div, sizes2,
		   //keyAddr, column1Addr, column2Addr, column3Addr,
			  //outputKeyAddr, outputColumn1Addr, outputColumn2Addr, outputColumn3Addr);

   //printf("Getting Data from FPGA...\n");
   //get_chunk_from_fpga((uint32_t*) outputKey, outputKeyAddr, 32);
   //get_chunk_from_fpga((uint32_t*) outputColumn1, outputColumn1Addr, 32);
   //get_chunk_from_fpga((uint32_t*) outputColumn2, outputColumn2Addr, 32);
   //get_chunk_from_fpga((uint32_t*) outputColumn4, outputColumn3Addr, 128);

   //printf("Checking Output for Correctness...\n");
   //for (int i = 0; i < num_div + 1; i++) {
       //int count = partition_get_count(0, i);
       //COMPARE(i, counts[i], count, "partition counts");
   //}
   //for (int i = 0; i < NUM_ELEMENTS; i++) {
       //COMPARE(i, e_startpos[i], outputKey[i], "key");
       //COMPARE(i, e_endpos[i], outputColumn1[i], "col1");
       //COMPARE(i, e_mapq[i], outputColumn2[i], "col2");
   //}
   //for (int i = 0; i < 2 * NUM_ELEMENTS; i++) {
       //COMPARE(i, e_grpname[i], outputColumn4[i], "col3");
   //}


   // Third Iteration: key=startpos, columns=cigar1,cigar2,cigar3
   //printf("Transferring Data to FPGA...\n");
   //transfer_chunk_to_fpga((uint32_t*)cigar1, column1Addr, 256);
   //transfer_chunk_to_fpga((uint32_t*)cigar2, column2Addr, 256);
   //transfer_chunk_to_fpga((uint32_t*)cigar3, column3Addr, 256);

   int sizes3[4] = {SIZE_64, SIZE_256, SIZE_256, SIZE_256};
   //partitionRun(num_col, num_div, sizes3,
           //keyAddr, column1Addr, column2Addr, column3Addr,
              //outputKeyAddr, outputColumn1Addr, outputColumn2Addr, outputColumn3Addr);

   //printf("Getting Data from FPGA...\n");
   //get_chunk_from_fpga((uint32_t*) outputKey, outputKeyAddr, 64);
   //get_chunk_from_fpga((uint32_t*) outputColumn4, outputColumn1Addr, 256);
   //get_chunk_from_fpga((uint32_t*) outputColumn5, outputColumn2Addr, 256);
   //get_chunk_from_fpga((uint32_t*) outputColumn6, outputColumn3Addr, 256);

   //printf("Checking Output for Correctness\n");
   //for (int i = 0; i < num_div + 1; i++) {
       //int count = partition_get_count(0, i);
       //COMPARE(i, counts[i], count, "partition counts");
   //}
   //for (int i = 0; i < NUM_ELEMENTS; i++) {
       //COMPARE(i, e_startpos[i], outputKey[i], "key");
   //}
   //for (int i = 0; i < 4 * NUM_ELEMENTS; i++) {
       //COMPARE(i, e_cigar1[i], outputColumn4[i], "col1");
       //COMPARE(i, e_cigar2[i], outputColumn5[i], "col2");
       //COMPARE(i, e_cigar3[i], outputColumn6[i], "col3");
   //}

   // Fourth Iteration: key=startpos, columns=base1,base2,base3
   printf("Transferring Data to FPGA...\n");
   transfer_chunk_to_fpga((uint32_t *) base1, column1Addr, 256);
   transfer_chunk_to_fpga((uint32_t *) base2, column2Addr, 256);
   transfer_chunk_to_fpga((uint32_t *) base3, column3Addr, 256);

   partitionRun(num_col, num_div, sizes3,
           keyAddr, column1Addr, column2Addr, column3Addr,
              outputKeyAddr, outputColumn1Addr, outputColumn2Addr, outputColumn3Addr);

   printf("Getting Data from FPGA...\n");
   get_chunk_from_fpga(outputKey, outputKeyAddr, 64);
   get_chunk_from_fpga(outputColumn4, outputColumn1Addr, 256);
   get_chunk_from_fpga(outputColumn5, outputColumn2Addr, 256);
   get_chunk_from_fpga(outputColumn6, outputColumn3Addr, 256);

   printf("Checking Output for Correctness\n");
   for (int i = 0; i < num_div + 1; i++) {
       int count = partition_get_count(0, i);
       COMPARE(i, counts[i], count, "partition counts");
   }
   for (int i = 0; i < NUM_ELEMENTS; i++) {
       COMPARE(i, e_startpos[i], outputKey[i], "key");
   }
   for (int i = 0; i < 4 * NUM_ELEMENTS; i++) {
       COMPARE(i, e_base1[i], outputColumn4[i], "col1");
       COMPARE(i, e_base2[i], outputColumn5[i], "col2");
       COMPARE(i, e_base3[i], outputColumn6[i], "col3");
   }

   // Fifth Iteration: key=startpos, columns=base4,base5,base6
   printf("Transferring Data to FPGA...\n");
   transfer_chunk_to_fpga(base4, column1Addr, 256);
   transfer_chunk_to_fpga(base5, column2Addr, 256);
   transfer_chunk_to_fpga(base6, column3Addr, 256);

   partitionRun(num_col, num_div, sizes3,
           keyAddr, column1Addr, column2Addr, column3Addr,
              outputKeyAddr, outputColumn1Addr, outputColumn2Addr, outputColumn3Addr);

   printf("Getting Data from FPGA...\n");
   get_chunk_from_fpga(outputKey, outputKeyAddr, 64);
   get_chunk_from_fpga(outputColumn4, outputColumn1Addr, 256);
   get_chunk_from_fpga(outputColumn5, outputColumn2Addr, 256);
   get_chunk_from_fpga(outputColumn6, outputColumn3Addr, 256);

   printf("Checking Output for Correctness\n");
   for (int i = 0; i < num_div + 1; i++) {
       int count = partition_get_count(0, i);
       COMPARE(i, counts[i], count, "partition counts");
   }
   for (int i = 0; i < NUM_ELEMENTS; i++) {
       COMPARE(i, e_startpos[i], outputKey[i], "key");
   }
   for (int i = 0; i < 4 * NUM_ELEMENTS; i++) {
       COMPARE(i, e_base4[i], outputColumn4[i], "col1");
       COMPARE(i, e_base5[i], outputColumn5[i], "col2");
       COMPARE(i, e_base6[i], outputColumn6[i], "col3");
   }

   //// Sixth Iteration: key=startpos, columns=base7, base8, prialign
   //printf("Transferring Data to FPGA...\n");
   //transfer_chunk_to_fpga((uint32_t*)base7, column1Addr, 256);
   //transfer_chunk_to_fpga((uint32_t*)base8, column2Addr, 256);
   //transfer_chunk_to_fpga((uint32_t*)prialign, column3Addr, 8);

   //int sizes4[4] = {SIZE_32, SIZE_256, SIZE_256, SIZE_8};
   //partitionRun(num_col, num_div, sizes4,
       //keyAddr, column1Addr, column2Addr, column3Addr,
        //outputKeyAddr, outputColumn1Addr, outputColumn2Addr, outputColumn3Addr);

   //printf("Getting Data from FPGA...\n");
   //get_chunk_from_fpga((uint32_t*) outputKey, outputKeyAddr, 32);
   //get_chunk_from_fpga((uint32_t*) outputColumn4, outputColumn1Addr, 256);
   //get_chunk_from_fpga((uint32_t*) outputColumn5, outputColumn2Addr, 256);
   //get_chunk_from_fpga((uint32_t*) outputColumn7, outputColumn3Addr, 8);

   //printf("Checking Output for Correctness\n");
   //for (int i = 0; i < num_div + 1; i++) {
       //int count = partition_get_count(0, i);
       //COMPARE(i, counts[i], count, "partition counts");
   //}
   //for (int i = 0; i < NUM_ELEMENTS; i++) {
       //COMPARE(i, e_startpos[i], outputKey[i], "key");
       //COMPARE(i, e_prialign[i], outputColumn7[i], "col3");
   //}
   //for (int i = 0; i < 4 * NUM_ELEMENTS; i++) {
       //COMPARE(i, e_base7[i], outputColumn4[i], "col1");
       //COMPARE(i, e_base8[i], outputColumn5[i], "col2");
   //}

   // Seventh Iteration: key=startpos, columns=qual1,qual2,qual3
   printf("Transferring Data to FPGA...\n");
   transfer_chunk_to_fpga(quality1, column1Addr, 256);
   transfer_chunk_to_fpga(quality2, column2Addr, 256);
   transfer_chunk_to_fpga(quality3, column3Addr, 256);

   partitionRun(num_col, num_div, sizes3,
       keyAddr, column1Addr, column2Addr, column3Addr,
        outputKeyAddr, outputColumn1Addr, outputColumn2Addr, outputColumn3Addr);

   printf("Getting Data from FPGA...\n");
   get_chunk_from_fpga(outputKey, outputKeyAddr, 64);
   get_chunk_from_fpga(outputColumn4, outputColumn1Addr, 256);
   get_chunk_from_fpga(outputColumn5, outputColumn2Addr, 256);
   get_chunk_from_fpga(outputColumn6, outputColumn3Addr, 256);

   printf("Checking Output for Correctness\n");
   for (int i = 0; i < num_div + 1; i++) {
       int count = partition_get_count(0, i);
       COMPARE(i, counts[i], count, "partition counts");
   }
   for (int i = 0; i < NUM_ELEMENTS; i++) {
       COMPARE(i, e_startpos[i], outputKey[i], "key");
   }
   for (int i = 0; i < 4 * NUM_ELEMENTS; i++) {
       COMPARE(i, e_qual1[i], outputColumn4[i], "col1");
       COMPARE(i, e_qual2[i], outputColumn5[i], "col2");
       COMPARE(i, e_qual3[i], outputColumn6[i], "col3");
   }

   // Eighth Iteration: key=startpos, columns=qual4,qual5,qual6
   printf("Transferring Data to FPGA...\n");
   transfer_chunk_to_fpga(quality4, column1Addr, 256);
   transfer_chunk_to_fpga(quality5, column2Addr, 256);
   transfer_chunk_to_fpga(quality6, column3Addr, 256);

   partitionRun(num_col, num_div, sizes3,
       keyAddr, column1Addr, column2Addr, column3Addr,
        outputKeyAddr, outputColumn1Addr, outputColumn2Addr, outputColumn3Addr);

   printf("Getting Data from FPGA...\n");
   get_chunk_from_fpga(outputKey, outputKeyAddr, 64);
   get_chunk_from_fpga(outputColumn4, outputColumn1Addr, 256);
   get_chunk_from_fpga(outputColumn5, outputColumn2Addr, 256);
   get_chunk_from_fpga(outputColumn6, outputColumn3Addr, 256);

   printf("Checking Output for Correctness\n");
   for (int i = 0; i < num_div + 1; i++) {
       int count = partition_get_count(0, i);
       COMPARE(i, counts[i], count, "partition counts");
   }
   for (int i = 0; i < NUM_ELEMENTS; i++) {
       COMPARE(i, e_startpos[i], outputKey[i], "key");
   }
   for (int i = 0; i < 4 * NUM_ELEMENTS; i++) {
       COMPARE(i, e_qual4[i], outputColumn4[i], "col1");
       COMPARE(i, e_qual5[i], outputColumn5[i], "col2");
       COMPARE(i, e_qual6[i], outputColumn6[i], "col3");
   }

   //// Ninth Iteration: key=startpos, columns=qual7,qual8
   //printf("Transferring Data to FPGA...\n");
   //transfer_chunk_to_fpga((uint32_t*)quality7, column1Addr, 256);
   //transfer_chunk_to_fpga((uint32_t*)quality8, column2Addr, 256);

   //partitionRun(2, num_div, sizes3,
       //keyAddr, column1Addr, column2Addr, column3Addr,
        //outputKeyAddr, outputColumn1Addr, outputColumn2Addr, outputColumn3Addr);

   //printf("Getting Data from FPGA...\n");
   //get_chunk_from_fpga((uint32_t*) outputKey, outputKeyAddr, 32);
   //get_chunk_from_fpga((uint32_t*) outputColumn4, outputColumn1Addr, 256);
   //get_chunk_from_fpga((uint32_t*) outputColumn5, outputColumn2Addr, 256);

   //printf("Checking Output for Correctness\n");
   //for (int i = 0; i < num_div + 1; i++) {
       //int count = partition_get_count(0, i);
       //COMPARE(i, counts[i], count, "partition counts");
   //}
   //for (int i = 0; i < NUM_ELEMENTS; i++) {
       //COMPARE(i, e_startpos[i], outputKey[i], "key");
   //}
   //for (int i = 0; i < 4 * NUM_ELEMENTS; i++) {
       //COMPARE(i, e_qual7[i], outputColumn4[i], "col1");
       //COMPARE(i, e_qual8[i], outputColumn5[i], "col2");
   //}

}
