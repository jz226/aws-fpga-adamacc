// Amazon FPGA Hardware Development Kit
//
// Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
//
// Licensed under the Amazon Software License (the "License"). You may not use
// this file except in compliance with the License. A copy of the License is
// located at
//
//    http://aws.amazon.com/asl/
//
// or in the "license" file accompanying this file. This file is distributed on
// an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or
// implied. See the License for the specific language governing permissions and
// limitations under the License.

#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>

#include <errno.h>
#include <signal.h>

// Vivado does not support svGetScopeFromName
#ifdef INCLUDE_DPI_CALLS
#ifndef VIVADO_SIM
#include "svdpi.h"
#endif
#endif

#include "sh_dpi_tasks.h"

void my_sig_handler (int sig)
{
    log_printf("interrupted by user, exiting...\n");
}

void test_main(uint32_t *exit_code) {

  // Vivado does not support svGetScopeFromName
#ifdef INCLUDE_DPI_CALLS
#ifndef VIVADO_SIM
  svScope scope;
#endif
#endif

  uint32_t rdata;

  // Vivado does not support svGetScopeFromName
#ifdef INCLUDE_DPI_CALLS
#ifndef VIVADO_SIM
  scope = svGetScopeFromName("tb");
  svSetScope(scope);
#endif
#endif

  // set a handler so we can break out of the sim
  struct sigaction action_opts;
  action_opts.sa_handler = my_sig_handler;
  action_opts.sa_flags = 0;
  sigaction(SIGINT, &my_sig_handler, 0);

  /* setup pipes */
  char * user = getenv("USER");
  char driver_to_xsim[1024];
  char xsim_to_driver[1024];
  sprintf(driver_to_xsim, "/tmp/%s_driver_to_xsim", user);
  sprintf(xsim_to_driver, "/tmp/%s_xsim_to_driver", user);
  mkfifo(xsim_to_driver, 0666);
  log_printf("opening driver_to_xsim\n");
  int driver_to_xsim_fd = open(driver_to_xsim, O_RDONLY);
  log_printf("opening xsim_to_driver\n"); 
  int xsim_to_driver_fd = open(xsim_to_driver, O_WRONLY);

  char buf[8];
  while(1) {
    int readbytes = read(driver_to_xsim_fd, buf, 8);
    if (readbytes == -1 && errno == EINTR) {
        continue;
    }
    if (readbytes != 8 ) {
      if (readbytes != 0) {
          log_printf("only read %d bytes\n", readbytes);
          break;
      }
      continue;
    }
    uint64_t cmd = *((uint64_t*)buf);
    if (cmd >> 63) {
      //write
      uint32_t addr = (cmd >> 32) & 0x7FFFFFFF;
      uint32_t data = cmd & 0xFFFFFFFF;
      //log_printf("write addr, data: %#x, %#x\n", addr, data);
      cl_poke_ocl(addr, data);
    } else {
      // read
      uint32_t addr = cmd & 0xFFFFFFFF;
      uint32_t dat;
      //log_printf("read addr: %#x\n", addr);
      cl_peek_ocl(addr, &dat);
      uint64_t ret = dat;
      //log_printf("read data: %#x\n", dat);
      write(xsim_to_driver_fd, (char*)&ret, 8);
    }
  }
  *exit_code = 0;
}
