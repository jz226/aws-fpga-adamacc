/*
 * Copyright (c) 2019,
 * The University of California, Berkeley and Duke University.
 * All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <array>
#include <vector>
#include <list>
#include <iostream>
#include <cassert>
#include <fstream>
#include <chrono>
#include <algorithm>
#include <sys/mman.h>
#include <sys/stat.h>
#include <cstring>
#include <cstdint>
#include <stdbool.h>
#include <sys/time.h>
#include <unistd.h>
#include <bitset>
#include "include/testhelp.h"
#include "include/butil.h"
#include "include/memrocc.h"
using namespace std;

#define LEN_QUAL 160
#define NCORE 16
//#define LINENUM 80000000
#define LINENUM 80

#define REDUCE_OP 0
#define COMP_OP 4
#define COMP_VAL 0

static long get_elapsed(struct timespec* start, struct timespec* end);

void help_text() {
    printf("Wrong input\n");
}

// Parameter mapping
// core id
// num_elems == lines
// inputAddrs == reduceInAddr
// outputAddr == sumQualAddr
void runReduceQuality(simif_f1_t *handle, int core, int lines, 
                      uint64_t inputAddrs, uint64_t outputAddr) {
    printf("Setting up reduce unit...\n");
    int comp = COMP_OP;
    int op = REDUCE_OP;
    int val = COMP_VAL;

    // reducer has one read channel and one write channel
    // the index of the ith read channel is (i)
    // the index of the jth write channel is (number_of_read_channels + j),
    // or (j + 1) for reducer
    reduce_set_addr(handle, 0, (void*) inputAddrs, core); // read channel 0
    reduce_set_addr(handle, 1, (void*) outputAddr, core); // write channel 0
    reduce_set_comparator(handle, comp, val, core);    // compOp, compVal
    reduce_set_op_and_cols(handle, op, LEN_QUAL, core);    // reduce_op, num_cols, 0
    reduce_set_len_and_nCores(handle, lines, NCORE, core);
    uint32_t temp = (LEN_QUAL / 4);
    uint32_t readLength = temp * lines;
    reduce_start(handle, core, readLength, OPCODE_REDUCER);
}


struct quality_struct {
    char qual[LEN_QUAL];
};
typedef struct quality_struct qualStruct;

void handlefield(char* fbegin, uint64_t* i, char* loc) {
    uint64_t offsetlength = 0;
    while (fbegin[*i] != '|') {
        offsetlength++;
        (*i)++;
    }
    memcpy(loc, &fbegin[*i - offsetlength], (size_t) offsetlength);
    loc[offsetlength] = '\0';
    (*i)++;//skip over '|'
}

int main(int argc, char** argv) {
//    if (argc == 2 && (!strcmp(argv[1], "--help") || !strcmp(argv[1], "-h"))) {
//        help_text();
//        exit(1);
//    }
//    if (argc != 3) {
//        help_text();
//        exit(1);
//    }

    struct timespec alpha, indone, omega, fpga_start;

    std::vector<qualStruct*> quals = std::vector<qualStruct*>();

    uint64_t lines = 0;
   
    struct timespec start, end;
    long transfer_time, run_time, rocc_sending_time, get_response_time;
    // uint64_t cmdAddr = static_balloc(NCORE * 6 + 1, 8 * sizeof(uint32_t));

    //***** SETTING UP REDUCER INPUT ADDRS
    uint64_t reduceInAddr = static_balloc(LINENUM, LEN_QUAL * sizeof(uint8_t));
    uint64_t sumQualAddr = static_balloc(LINENUM, sizeof(uint64_t));
    uint64_t reduceBytes = LEN_QUAL * sizeof(uint8_t) * LINENUM;
    uint8_t* columns2 = (uint8_t*) calloc(LINENUM, LEN_QUAL*sizeof(uint8_t));  // 150B * lines

    uint64_t len = (uint64_t) LEN_QUAL * LINENUM;

    for(uint64_t i = 0; i < len; i++) {
        *(columns2 + i) = (uint8_t)(i%LEN_QUAL+1);
    }
    uint8_t* reduceInputs = (uint8_t*) columns2; // Quality Score columns are reduceInputs

    simif_f1_t* handle = new simif_f1_t(0);

    //***** START REDUCER ON FPGA
    getcl(fpga_start);
    getcl(start);
    printf("Transferring array to FPGA...\n");
    transfer_chunk_to_fpga(handle, (uint32_t*)reduceInputs, reduceInAddr, reduceBytes);
    printf("\n");
    fsync(handle->get_write_fd());
    getcl(end);
    printf("Data transfer for reducer took: ");
    transfer_time = get_elapsed(&start, &end);
    printf("Waiting for response from Reducer...\n");

    printf("Copied back\n");
    uint8_t* qualCopy = (uint8_t*) calloc(LINENUM, LEN_QUAL * sizeof(uint8_t));
    get_chunk_from_fpga(handle, (uint32_t*) qualCopy, reduceInAddr, reduceBytes);
    fsync(handle->get_read_fd());
    printf("Read 0\n");
    for(uint64_t j = 0; j < LEN_QUAL; j++) {
       cout << (int)*((qualCopy +j)) << " "; 
    }
    printf("\n");

    for(uint64_t i = LINENUM-1; i < LINENUM; i++) {
       printf("Read %i\n",i);
       for(uint64_t j = 0; j < LEN_QUAL; j++) {
           cout << (int)*((qualCopy + i * LEN_QUAL+j)) << " "; 
       }
       printf("\n");
    }
    free(qualCopy);
    
    assert(LINENUM % NCORE == 0);
    getcl(start);

    uint64_t inAddressForCore = reduceInAddr;
    uint64_t outAddressForCore = sumQualAddr;
    uint64_t linesPerCore = LINENUM / NCORE;
    for (int i = 0; i < NCORE; i++) {
        runReduceQuality(handle, i, linesPerCore, inAddressForCore, outAddressForCore);
        inAddressForCore  += LEN_QUAL * linesPerCore;
        outAddressForCore += 8 * linesPerCore;
    }

    // encode_cmd_buf_xs_mem(handle, CUSTOM_3, 16, 1, 1, OPCODE_REDUCER, 0, false, false, true);
    // uint64_t cmdToExec = numCmd;
    // send_cmd_buf(handle, cmdAddr);
    // exec_cmd(handle, cmdAddr, cmdToExec);
    getcl(end);
    rocc_sending_time = get_elapsed(&start, &end);
    getcl(start);
    uint res_count = 0;
    std::pair<uint32_t, uint32_t> resp = get_id_retval(handle);
    cout << "ID : " << resp.first << " ERROR : " << resp.second << "\n";
    getcl(end);
    get_response_time = get_elapsed(&start, &end);
    run_time = get_elapsed(&fpga_start, &end);
    printf("Reducer took: ");
    printf("Total Data Transfer Time: %ld\n", transfer_time);
    cout << "Rocc_sending_time time: " << rocc_sending_time << endl;
    cout << "Get response_time time: " << get_response_time << endl;
    printf("Total Run Time: %ld\n", run_time);

    uint32_t* accum = (uint32_t*) calloc(LINENUM, sizeof(uint32_t) * 2);
    get_chunk_from_fpga(handle, (uint32_t*) accum, sumQualAddr, LINENUM * sizeof(uint32_t)*2);
    fsync(handle->get_read_fd());
    for (int i = 0; i < LINENUM; i++ ) {
        uint64_t result = *(uint32_t*)(accum + 2 * i + 0);
        printf("%ld", result);
    }
    free(accum);
    fflush(stdout);
    delete handle;
    return 1;
}

static long get_elapsed(struct timespec* start, struct timespec* end) {
    long sec_diff, nsec_diff;
    if (end->tv_sec >= start->tv_sec) {
        sec_diff = end->tv_sec - start->tv_sec;
        if (end->tv_nsec >= start->tv_nsec) {
            nsec_diff = end->tv_nsec - start->tv_nsec;
            return sec_diff * 1000000000L + nsec_diff;
        } else {
            nsec_diff = start->tv_nsec - end->tv_nsec;
            return sec_diff * 1000000000L - nsec_diff;
        }
    } else {
        return 0;
    }
}
