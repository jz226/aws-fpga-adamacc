/*
 * Copyright (c) 2019,
 * The University of California, Berkeley and Duke University.
 * All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "simif_f1.h"

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

simif_f1_t::simif_f1_t(int id) {
#ifdef SIMULATION_XSIM
  char* user = getenv("USER");
  sprintf(driver_to_xsim, "/tmp/%s_driver_to_xsim", user);
  sprintf(xsim_to_driver, "/tmp/%s_xsim_to_driver", user);
  mkfifo(driver_to_xsim, 0666);
  fprintf(stderr, "opening driver to xsim\n");
  driver_to_xsim_fd = open(driver_to_xsim, O_WRONLY);
  fprintf(stderr, "opening xsim to driver\n");
  xsim_to_driver_fd = open(xsim_to_driver, O_RDONLY);
#else
  fpga_setup(id);
  
  char write_file_name[256];
  char read_file_name[256];
  sprintf(write_file_name, "/dev/xdma%d_h2c_0", id);
  sprintf(read_file_name, "/dev/xdma%d_c2h_0", id);
  if ((xdma_write_fd = open(write_file_name,O_WRONLY)) == -1) {
    perror("xdma h2c fd failed to open");
    exit(1);
  } else {
    printf("opened xdma h2c fd = %d\n", xdma_write_fd);
  }
  if ((xdma_read_fd = open(read_file_name,O_RDONLY)) == -1) {
    perror("xdma c2h fd failed to open");
    exit(1);
  } else {
    printf("opened xdma  c2h fd = %d\n", xdma_read_fd);
  }
#endif
}

void simif_f1_t::check_rc(int rc, char * infostr) {
#ifndef SIMULATION_XSIM
  if (rc) {
    if (infostr) {
      fprintf(stderr, "%s\n", infostr);
    }
    fprintf(stderr, "INVALID RETCODE: %d\n", rc, infostr);
    fpga_shutdown();
    exit(1);
  }
#endif
}

void simif_f1_t::fpga_shutdown() {

#ifndef SIMULATION_XSIM
  int rc = fpga_pci_detach(pci_bar_handle);
  // don't call check_rc because of fpga_shutdown call. do it manually:
  if (rc) {
    fprintf(stderr, "Failure while detaching from the fpga: %d\n", rc);
  }
#endif
}

void simif_f1_t::fpga_setup(int id) {
#ifndef SIMULATION_XSIM
  /*
   * pci_vendor_id and pci_device_id values below are Amazon's and avaliable
   * to use for a given FPGA slot.
   * Users may replace these with their own if allocated to them by PCI SIG
   */
  uint16_t pci_vendor_id = 0x1D0F; /* Amazon PCI Vendor ID */
  uint16_t pci_device_id = 0xF000; /* PCI Device ID preassigned by Amazon for F1 applications */

  slot_id = id;
  int rc = fpga_pci_init();
  check_rc(rc, "fpga_pci_init FAILED");

  /* check AFI status */
  struct fpga_mgmt_image_info info = {0}; 

  /* get local image description, contains status, vendor id, and device id. */
  rc = fpga_mgmt_describe_local_image(slot_id, &info,0);
  check_rc(rc, "Unable to get AFI information from slot. Are you running as root?");

  /* check to see if the slot is ready */
  if (info.status != FPGA_STATUS_LOADED) {
    rc = 1;
    check_rc(rc, "AFI in Slot is not in READY state !");
  }

  fprintf(stderr, "AFI PCI  Vendor ID: 0x%x, Device ID 0x%x\n",
	  info.spec.map[FPGA_APP_PF].vendor_id,
	  info.spec.map[FPGA_APP_PF].device_id);

  /* confirm that the AFI that we expect is in fact loaded */
  if (info.spec.map[FPGA_APP_PF].vendor_id != pci_vendor_id ||
      info.spec.map[FPGA_APP_PF].device_id != pci_device_id) {
    fprintf(stderr, "AFI does not show expected PCI vendor id and device ID. If the AFI "
	    "was just loaded, it might need a rescan. Rescanning now.\n");

    rc = fpga_pci_rescan_slot_app_pfs(slot_id);
    check_rc(rc, "Unable to update PF for slot");
    /* get local image description, contains status, vendor id, and device id. */
    rc = fpga_mgmt_describe_local_image(slot_id, &info,0);
    check_rc(rc, "Unable to get AFI information from slot");

    fprintf(stderr, "AFI PCI  Vendor ID: 0x%x, Device ID 0x%x\n",
            info.spec.map[FPGA_APP_PF].vendor_id,
            info.spec.map[FPGA_APP_PF].device_id);

    /* confirm that the AFI that we expect is in fact loaded after rescan */
    if (info.spec.map[FPGA_APP_PF].vendor_id != pci_vendor_id ||
	info.spec.map[FPGA_APP_PF].device_id != pci_device_id) {
      rc = 1;
      check_rc(rc, "The PCI vendor id and device of the loaded AFI are not "
	       "the expected values.");
    }
  }

  /* attach to BAR0 */
  pci_bar_handle = PCI_BAR_HANDLE_INIT;
  rc = fpga_pci_attach(slot_id, FPGA_APP_PF, APP_PF_BAR0, 0, &pci_bar_handle);
  check_rc(rc, "fpga_pci_attach FAILED");
#endif
}



simif_f1_t::~simif_f1_t() {
#ifdef SIMULATION_XSIM
  int exit = 0x12459333;
  ::write(driver_to_xsim_fd, (char*) &exit, 4);
  close(xsim_to_driver_fd);
  close(driver_to_xsim_fd);
#else
  if (close(xdma_write_fd) < 0) {
    perror("failed to close xdma h2c");
    exit(1);
  }
  if (close(xdma_read_fd) < 0) {
    perror("failed to close xdma c2h");
    exit(1);
  }
  fpga_shutdown();
#endif
}

void simif_f1_t::store_resp(uint32_t rd, uint32_t unit_id, uint32_t retval) {
  rocc_resp_table[rd][unit_id] = retval;
}
  
uint32_t simif_f1_t::resp_lookup(uint32_t rd, uint32_t unit_id) {
  return rocc_resp_table[rd][unit_id];
}

int simif_f1_t::get_write_fd() {
#ifdef SIMULATION_XSIM
  return int(NULL);
#else  
  return xdma_write_fd;
#endif  
}
int simif_f1_t::get_read_fd() {
#ifdef SIMULATION_XSIM
  return int(NULL);
#else  
  return xdma_read_fd;
#endif  
}

void simif_f1_t::write(size_t addr, uint32_t data) {
  // addr is really a (32-byte) word address because of zynq implementation
  addr <<= 2;
#ifdef SIMULATION_XSIM
  // printf("addr, data: %#x, %#x\n", addr, data);
  uint64_t cmd = (((uint64_t)(0x80000000 | addr)) << 32) | (uint64_t)data;
  char * buf = (char*)&cmd;
  ::write(driver_to_xsim_fd, buf, 8);
#else
  int rc = fpga_pci_poke(pci_bar_handle, addr, data);
  check_rc(rc, NULL);
#endif
}

uint32_t simif_f1_t::read(size_t addr) {
  addr <<= 2;
#ifdef SIMULATION_XSIM
  // printf("addr: %#x\n", addr);
  uint64_t cmd = addr;
  char * buf = (char*)&cmd;
  ::write(driver_to_xsim_fd, buf, 8);

  int gotdata = 0;
  while (gotdata == 0) {
    gotdata = ::read(xsim_to_driver_fd, buf, 8);
    if (gotdata != 0 && gotdata != 8) {
      printf("ERR GOTDATA %d\n", gotdata);
    }
  }
  // printf("data: %#x\n", *((uint64_t*)buf));
  return *((uint64_t*)buf);
#else
  uint32_t value;
  int rc = fpga_pci_peek(pci_bar_handle, addr, &value);
  return value & 0xFFFFFFFF;
#endif
}


uint32_t simif_f1_t::is_write_ready() {
  uint64_t addr = 0x4;
#ifdef SIMULATION_XSIM
  uint64_t cmd = addr;
  char * buf = (char*)&cmd;
  ::write(driver_to_xsim_fd, buf, 8);

  int gotdata = 0;
  while (gotdata == 0) {
    gotdata = ::read(xsim_to_driver_fd, buf, 8);
    if (gotdata != 0 && gotdata != 8) {
      printf("ERR GOTDATA %d\n", gotdata);
    }
  }
  return *((uint64_t*)buf);
#else
  uint32_t value;
  int rc = fpga_pci_peek(pci_bar_handle, addr, &value);
  check_rc(rc, NULL);
  return value & 0xFFFFFFFF;
#endif
}
