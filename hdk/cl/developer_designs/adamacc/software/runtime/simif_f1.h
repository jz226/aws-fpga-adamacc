/*
 * Copyright (c) 2019,
 * The University of California, Berkeley and Duke University.
 * All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __SIMIF_F1_H
#define __SIMIF_F1_H

//#include "simif.h"    // from midas
#include <cstring>
#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <map>

#ifndef SIMULATION_XSIM
#include <fpga_pci.h>
#include <fpga_mgmt.h>
#endif

#define MMIO_WIDTH 8

class simif_f1_t
{
 public:
  simif_f1_t(int id = 0);
  virtual ~simif_f1_t();
  virtual void write(size_t addr, uint32_t data);
  virtual uint32_t read(size_t addr);
  uint32_t is_write_ready();
  void check_rc(int rc, char * infostr);
  void fpga_shutdown();
  void fpga_setup(int id = 0);
  int get_write_fd();
  int get_read_fd();
  void store_resp(uint32_t rd, uint32_t unit_id, uint32_t retval);  
  uint32_t resp_lookup(uint32_t rd, uint32_t unit_id);  
 private:
  char in_buf[MMIO_WIDTH];
  char out_buf[MMIO_WIDTH];
  std::map< uint32_t , std::map<uint32_t,uint32_t> > rocc_resp_table;
#ifdef SIMULATION_XSIM
  char driver_to_xsim[1024];
  char xsim_to_driver[1024];
  int driver_to_xsim_fd;
  int xsim_to_driver_fd;
#else
  //    int rc;
  int slot_id;
  pci_bar_handle_t pci_bar_handle;
  int xdma_write_fd;
  int xdma_read_fd;
#endif
};

#endif // __SIMIF_F1_H
