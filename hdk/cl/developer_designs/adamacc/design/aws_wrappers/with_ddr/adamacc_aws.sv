// Amazon FPGA Hardware Development Kit
//
// Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
//
// Licensed under the Amazon Software License (the "License"). You may not use
// this file except in compliance with the License. A copy of the License is
// located at
//
//    http://aws.amazon.com/asl/
//
// or in the "license" file accompanying this file. This file is distributed on
// an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or
// implied. See the License for the specific language governing permissions and
// limitations under the License.
`include "adamacc_defines.vh"
`include "cl_id_defines.vh"

module adamacc_aws #(parameter NUM_PCIE=1, parameter NUM_DDR=1, parameter NUM_HMC=4, parameter NUM_GTY = 4) 

(
   `include "cl_ports.vh" // Fixed port definition

);
  
   localparam NUM_CFG_STGS_INT_TST = 4;
   localparam NUM_CFG_STGS_HMC_ATG = 4;
   localparam NUM_CFG_STGS_CL_DDR_ATG = 4;
   localparam NUM_CFG_STGS_SH_DDR_ATG = 4;
   localparam NUM_CFG_STGS_PCIE_ATG = 4;
   localparam NUM_CFG_STGS_AURORA_ATG = 4;
   localparam NUM_CFG_STGS_XDCFG = 4;
   localparam NUM_CFG_STGS_XDMA = 4;
   
`ifdef SIM
   localparam DDR_SCRB_MAX_ADDR = 64'h1FFF;
`else   
   localparam DDR_SCRB_MAX_ADDR = 64'h3FFFFFFFF; //16GB 
`endif
   localparam DDR_SCRB_BURST_LEN_MINUS1 = 15;
   localparam HMC_SCRB_BURST_LEN_MINUS1 = 3;

`include "unused_ddr_a_b_d_template.inc"
//`include "unused_ddr_c_template.inc"
`include "unused_pcim_template.inc"
//`include "unused_dma_pcis_template.inc"
`include "unused_flr_template.inc"
`include "unused_apppf_irq_template.inc"
`include "unused_cl_sda_template.inc"
`include "unused_sh_bar1_template.inc"
//-------------------------------------------------
// Reset Synchronization
//-------------------------------------------------
logic pre_sync_rst_main_n;
logic sync_rst_main_n;
logic pre_sync_rst_n_extra;
logic sync_rst_extra_n;
   
always_ff @(negedge rst_main_n or posedge clk_main_a0)
   if (!rst_main_n)
   begin
      pre_sync_rst_main_n <= 0;
      sync_rst_main_n <= 0;
   end
   else
   begin
      pre_sync_rst_main_n <= 1;
      sync_rst_main_n <= pre_sync_rst_main_n;
   end

always_ff @(negedge rst_main_n or posedge clk_extra_a1)
   if (!rst_main_n)
   begin
      pre_sync_rst_n_extra  <= 0;
      sync_rst_extra_n <= 0;
   end
   else
   begin
      pre_sync_rst_n_extra  <= 1;
      sync_rst_extra_n <= pre_sync_rst_n_extra;
   end

(* dont_touch = "true" *) logic sh_ddr_sync_rst_n;
lib_pipe #(.WIDTH(1), .STAGES(4)) SH_DDR_SLC_RST_N (.clk(clk_main_a0), .rst_n(1'b1), .in_bus(sync_rst_main_n), .out_bus(sh_ddr_sync_rst_n));

//-------------------------------------------
// Wires for generated verilog
//-------------------------------------------

  //PCI ocl AXI
  wire          io_ocl_aw_ready;
  wire          io_ocl_aw_valid;
  wire [31:0]   io_ocl_aw_bits_addr;
  wire [7:0]    io_ocl_aw_bits_len;
  wire [2:0]    io_ocl_aw_bits_size;
  wire [1:0]    io_ocl_aw_bits_burst;
  wire          io_ocl_aw_bits_lock;
  wire [3:0]    io_ocl_aw_bits_cache;
  wire [2:0]    io_ocl_aw_bits_prot;
  wire [3:0]    io_ocl_aw_bits_qos;
  wire [3:0]    io_ocl_aw_bits_region;
  wire [9:0]    io_ocl_aw_bits_id;
  wire          io_ocl_aw_bits_user;
  wire          io_ocl_w_ready;
  wire          io_ocl_w_valid;
  wire [31:0]   io_ocl_w_bits_data;
  wire          io_ocl_w_bits_last;
  wire [9:0]    io_ocl_w_bits_id;
  wire [3:0]    io_ocl_w_bits_strb;
  wire          io_ocl_w_bits_user;
  wire          io_ocl_b_ready;
  wire          io_ocl_b_valid;
  wire [1:0]    io_ocl_b_bits_resp;
  wire [9:0]    io_ocl_b_bits_id;
  wire          io_ocl_b_bits_user;
  wire          io_ocl_ar_ready;
  wire          io_ocl_ar_valid;
  wire [31:0]   io_ocl_ar_bits_addr;
  wire [7:0]    io_ocl_ar_bits_len;
  wire [2:0]    io_ocl_ar_bits_size;
  wire [1:0]    io_ocl_ar_bits_burst;
  wire          io_ocl_ar_bits_lock;
  wire [3:0]    io_ocl_ar_bits_cache;
  wire [2:0]    io_ocl_ar_bits_prot;
  wire [3:0]    io_ocl_ar_bits_qos;
  wire [3:0]    io_ocl_ar_bits_region;
  wire [9:0]    io_ocl_ar_bits_id;
  wire          io_ocl_ar_bits_user;
  wire          io_ocl_r_ready;
  wire          io_ocl_r_valid;
  wire [1:0]    io_ocl_r_bits_resp;
  wire [31:0]   io_ocl_r_bits_data;
  wire          io_ocl_r_bits_last;
  wire [9:0]    io_ocl_r_bits_id;
  wire          io_ocl_r_bits_user;


  //DDR AXI
  wire          io_mem_0_aw_ready;
  wire          io_mem_0_aw_valid;
  wire [49:0]   io_mem_0_aw_bits_addr;
  wire [7:0]    io_mem_0_aw_bits_len;
  wire [2:0]    io_mem_0_aw_bits_size;
  wire [1:0]    io_mem_0_aw_bits_burst;
  wire          io_mem_0_aw_bits_lock;
  wire [3:0]    io_mem_0_aw_bits_cache;
  wire [2:0]    io_mem_0_aw_bits_prot;
  wire [3:0]    io_mem_0_aw_bits_qos;
  wire [3:0]    io_mem_0_aw_bits_region;
  wire [15:0]   io_mem_0_aw_bits_id;
  wire          io_mem_0_aw_bits_user;
  wire          io_mem_0_w_ready;
  wire          io_mem_0_w_valid;
  wire [511:0]  io_mem_0_w_bits_data;
  wire          io_mem_0_w_bits_last;
  wire [15:0]   io_mem_0_w_bits_id;
  wire [63:0]   io_mem_0_w_bits_strb;
  wire          io_mem_0_w_bits_user;
  wire          io_mem_0_b_ready;
  wire          io_mem_0_b_valid;
  wire [1:0]    io_mem_0_b_bits_resp;
  wire [15:0]   io_mem_0_b_bits_id;
  wire          io_mem_0_b_bits_user;
  wire          io_mem_0_ar_ready;
  wire          io_mem_0_ar_valid;
  wire [49:0]   io_mem_0_ar_bits_addr;
  wire [7:0]    io_mem_0_ar_bits_len;
  wire [2:0]    io_mem_0_ar_bits_size;
  wire [1:0]    io_mem_0_ar_bits_burst;
  wire          io_mem_0_ar_bits_lock;
  wire [3:0]    io_mem_0_ar_bits_cache;
  wire [2:0]    io_mem_0_ar_bits_prot;
  wire [3:0]    io_mem_0_ar_bits_qos;
  wire [3:0]    io_mem_0_ar_bits_region;
  wire [15:0]   io_mem_0_ar_bits_id;
  wire          io_mem_0_ar_bits_user;
  wire          io_mem_0_r_ready;
  wire          io_mem_0_r_valid;
  wire [1:0]    io_mem_0_r_bits_resp;
  wire [511:0]  io_mem_0_r_bits_data;
  wire          io_mem_0_r_bits_last;
  wire [15:0]   io_mem_0_r_bits_id;
  wire          io_mem_0_r_bits_user;

  //DDR AXI POST REGISTER SLICE (DDR SIDE)
  logic          io_mem_0_aw_ready_q;
  logic          io_mem_0_aw_valid_q;
  logic [63:0]   io_mem_0_aw_bits_addr_q;
  logic [7:0]    io_mem_0_aw_bits_len_q;
  logic [2:0]    io_mem_0_aw_bits_size_q;
  logic [1:0]    io_mem_0_aw_bits_burst_q;
  logic          io_mem_0_aw_bits_lock_q;
  logic [3:0]    io_mem_0_aw_bits_cache_q;
  logic [2:0]    io_mem_0_aw_bits_prot_q;
  logic [3:0]    io_mem_0_aw_bits_qos_q;
  logic [3:0]    io_mem_0_aw_bits_region_q;
  logic [15:0]   io_mem_0_aw_bits_id_q;
  logic          io_mem_0_aw_bits_user_q;
  logic          io_mem_0_w_ready_q;
  logic          io_mem_0_w_valid_q;
  logic [511:0]  io_mem_0_w_bits_data_q;
  logic          io_mem_0_w_bits_last_q;
  logic [15:0]   io_mem_0_w_bits_id_q;
  logic [63:0]   io_mem_0_w_bits_strb_q;
  logic          io_mem_0_w_bits_user_q;
  logic          io_mem_0_b_ready_q;
  logic          io_mem_0_b_valid_q;
  logic [1:0]    io_mem_0_b_bits_resp_q;
  logic [15:0]   io_mem_0_b_bits_id_q;
  logic          io_mem_0_b_bits_user_q;
  logic          io_mem_0_ar_ready_q;
  logic          io_mem_0_ar_valid_q;
  logic [63:0]   io_mem_0_ar_bits_addr_q;
  logic [7:0]    io_mem_0_ar_bits_len_q;
  logic [2:0]    io_mem_0_ar_bits_size_q;
  logic [1:0]    io_mem_0_ar_bits_burst_q;
  logic          io_mem_0_ar_bits_lock_q;
  logic [3:0]    io_mem_0_ar_bits_cache_q;
  logic [2:0]    io_mem_0_ar_bits_prot_q;
  logic [3:0]    io_mem_0_ar_bits_qos_q;
  logic [3:0]    io_mem_0_ar_bits_region_q;
  logic [15:0]   io_mem_0_ar_bits_id_q;
  logic          io_mem_0_ar_bits_user_q;
  logic          io_mem_0_r_ready_q;
  logic          io_mem_0_r_valid_q;
  logic [1:0]    io_mem_0_r_bits_resp_q;
  logic [511:0]  io_mem_0_r_bits_data_q;
  logic          io_mem_0_r_bits_last_q;
  logic [15:0]   io_mem_0_r_bits_id_q;
  logic          io_mem_0_r_bits_user_q;

  wire [63:0]   io_mem_0_aw_bits_addr_full;
  wire [63:0]   io_mem_0_ar_bits_addr_full;
  assign io_mem_0_ar_bits_addr_full = {14'h0, io_mem_0_ar_bits_addr};
  assign io_mem_0_aw_bits_addr_full = {14'h0, io_mem_0_aw_bits_addr};

axi_register_slice ddr_slice_0 (
  .aclk(clk_main_a0),                      // input wire aclk
  .aresetn(sync_rst_main_n),                // input wire aresetn
  .s_axi_awid(io_mem_0_aw_bits_id),          // input wire [15 : 0] s_axi_awid
  .s_axi_awaddr(io_mem_0_aw_bits_addr_full),      // input wire [63 : 0] s_axi_awaddr
  .s_axi_awlen(io_mem_0_aw_bits_len),        // input wire [7 : 0] s_axi_awlen
  .s_axi_awsize(io_mem_0_aw_bits_size),      // input wire [2 : 0] s_axi_awsize
  .s_axi_awburst(io_mem_0_aw_bits_burst),    // input wire [1 : 0] s_axi_awburst
  .s_axi_awlock(io_mem_0_aw_bits_lock),      // input wire [0 : 0] s_axi_awlock
  .s_axi_awcache(io_mem_0_aw_bits_cache),    // input wire [3 : 0] s_axi_awcache
  .s_axi_awprot(io_mem_0_aw_bits_prot),      // input wire [2 : 0] s_axi_awprot
  .s_axi_awregion(io_mem_0_aw_bits_region),  // input wire [3 : 0] s_axi_awregion
  .s_axi_awqos(io_mem_0_aw_bits_qos),        // input wire [3 : 0] s_axi_awqos
  .s_axi_awvalid(io_mem_0_aw_valid),    // input wire s_axi_awvalid
  .s_axi_awready(io_mem_0_aw_ready),    // output wire s_axi_awready
  .s_axi_wdata(io_mem_0_w_bits_data),        // input wire [511 : 0] s_axi_wdata
  .s_axi_wstrb(io_mem_0_w_bits_strb),        // input wire [63 : 0] s_axi_wstrb
  .s_axi_wlast(io_mem_0_w_bits_last),        // input wire s_axi_wlast
  .s_axi_wvalid(io_mem_0_w_valid),      // input wire s_axi_wvalid
  .s_axi_wready(io_mem_0_w_ready),      // output wire s_axi_wready
  .s_axi_bid(io_mem_0_b_bits_id),            // output wire [15 : 0] s_axi_bid
  .s_axi_bresp(io_mem_0_b_bits_resp),        // output wire [1 : 0] s_axi_bresp
  .s_axi_bvalid(io_mem_0_b_valid),      // output wire s_axi_bvalid
  .s_axi_bready(io_mem_0_b_ready),      // input wire s_axi_bready
  .s_axi_arid(io_mem_0_ar_bits_id),          // input wire [15 : 0] s_axi_arid
  .s_axi_araddr(io_mem_0_ar_bits_addr_full),      // input wire [63 : 0] s_axi_araddr
  .s_axi_arlen(io_mem_0_ar_bits_len),        // input wire [7 : 0] s_axi_arlen
  .s_axi_arsize(io_mem_0_ar_bits_size),      // input wire [2 : 0] s_axi_arsize
  .s_axi_arburst(io_mem_0_ar_bits_burst),    // input wire [1 : 0] s_axi_arburst
  .s_axi_arlock(io_mem_0_ar_bits_lock),      // input wire [0 : 0] s_axi_arlock
  .s_axi_arcache(io_mem_0_ar_bits_cache),    // input wire [3 : 0] s_axi_arcache
  .s_axi_arprot(io_mem_0_ar_bits_prot),      // input wire [2 : 0] s_axi_arprot
  .s_axi_arregion(io_mem_0_ar_bits_region),  // input wire [3 : 0] s_axi_arregion
  .s_axi_arqos(io_mem_0_ar_bits_qos),        // input wire [3 : 0] s_axi_arqos
  .s_axi_arvalid(io_mem_0_ar_valid),    // input wire s_axi_arvalid
  .s_axi_arready(io_mem_0_ar_ready),    // output wire s_axi_arready
  .s_axi_rid(io_mem_0_r_bits_id),            // output wire [15 : 0] s_axi_rid
  .s_axi_rdata(io_mem_0_r_bits_data),        // output wire [511 : 0] s_axi_rdata
  .s_axi_rresp(io_mem_0_r_bits_resp),        // output wire [1 : 0] s_axi_rresp
  .s_axi_rlast(io_mem_0_r_bits_last),        // output wire s_axi_rlast
  .s_axi_rvalid(io_mem_0_r_valid),      // output wire s_axi_rvalid
  .s_axi_rready(io_mem_0_r_ready),      // input wire s_axi_rready
  .m_axi_awid(io_mem_0_aw_bits_id_q),          // output wire [15 : 0] m_axi_awid
  .m_axi_awaddr(io_mem_0_aw_bits_addr_q),      // output wire [63 : 0] m_axi_awaddr
  .m_axi_awlen(io_mem_0_aw_bits_len_q),        // output wire [7 : 0] m_axi_awlen
  .m_axi_awsize(io_mem_0_aw_bits_size_q),      // output wire [2 : 0] m_axi_awsize
  .m_axi_awburst(io_mem_0_aw_bits_burst_q),    // output wire [1 : 0] m_axi_awburst
  .m_axi_awlock(io_mem_0_aw_bits_lock_q),      // output wire [0 : 0] m_axi_awlock
  .m_axi_awcache(io_mem_0_aw_bits_cache_q),    // output wire [3 : 0] m_axi_awcache
  .m_axi_awprot(io_mem_0_aw_bits_prot_q),      // output wire [2 : 0] m_axi_awprot
  .m_axi_awregion(io_mem_0_aw_bits_region_q),  // output wire [3 : 0] m_axi_awregion
  .m_axi_awqos(io_mem_0_aw_bits_qos_q),        // output wire [3 : 0] m_axi_awqos
  .m_axi_awvalid(io_mem_0_aw_valid_q),    // output wire m_axi_awvalid
  .m_axi_awready(io_mem_0_aw_ready_q),    // input wire m_axi_awready
  .m_axi_wdata(io_mem_0_w_bits_data_q),        // output wire [511 : 0] m_axi_wdata
  .m_axi_wstrb(io_mem_0_w_bits_strb_q),        // output wire [63 : 0] m_axi_wstrb
  .m_axi_wlast(io_mem_0_w_bits_last_q),        // output wire m_axi_wlast
  .m_axi_wvalid(io_mem_0_w_valid_q),      // output wire m_axi_wvalid
  .m_axi_wready(io_mem_0_w_ready_q),      // input wire m_axi_wready
  .m_axi_bid(io_mem_0_b_bits_id_q),            // input wire [15 : 0] m_axi_bid
  .m_axi_bresp(io_mem_0_b_bits_resp_q),        // input wire [1 : 0] m_axi_bresp
  .m_axi_bvalid(io_mem_0_b_valid_q),      // input wire m_axi_bvalid
  .m_axi_bready(io_mem_0_b_ready_q),      // output wire m_axi_bready
  .m_axi_arid(io_mem_0_ar_bits_id_q),          // output wire [15 : 0] m_axi_arid
  .m_axi_araddr(io_mem_0_ar_bits_addr_q),      // output wire [63 : 0] m_axi_araddr
  .m_axi_arlen(io_mem_0_ar_bits_len_q),        // output wire [7 : 0] m_axi_arlen
  .m_axi_arsize(io_mem_0_ar_bits_size_q),      // output wire [2 : 0] m_axi_arsize
  .m_axi_arburst(io_mem_0_ar_bits_burst_q),    // output wire [1 : 0] m_axi_arburst
  .m_axi_arlock(io_mem_0_ar_bits_lock_q),      // output wire [0 : 0] m_axi_arlock
  .m_axi_arcache(io_mem_0_ar_bits_cache_q),    // output wire [3 : 0] m_axi_arcache
  .m_axi_arprot(io_mem_0_ar_bits_prot_q),      // output wire [2 : 0] m_axi_arprot
  .m_axi_arregion(io_mem_0_ar_bits_region_q),  // output wire [3 : 0] m_axi_arregion
  .m_axi_arqos(io_mem_0_ar_bits_qos_q),        // output wire [3 : 0] m_axi_arqos
  .m_axi_arvalid(io_mem_0_ar_valid_q),    // output wire m_axi_arvalid
  .m_axi_arready(io_mem_0_ar_ready_q),    // input wire m_axi_arready
  .m_axi_rid(io_mem_0_r_bits_id_q),            // input wire [15 : 0] m_axi_rid
  .m_axi_rdata(io_mem_0_r_bits_data_q),        // input wire [511 : 0] m_axi_rdata
  .m_axi_rresp(io_mem_0_r_bits_resp_q),        // input wire [1 : 0] m_axi_rresp
  .m_axi_rlast(io_mem_0_r_bits_last_q),        // input wire m_axi_rlast
  .m_axi_rvalid(io_mem_0_r_valid_q),      // input wire m_axi_rvalid
  .m_axi_rready(io_mem_0_r_ready_q)      // output wire m_axi_rready
);

//-------------------------------------------
// Instatiate generated verilog
//-------------------------------------------

CatapultTop generated_top (
  .clock(clk_main_a0),
  .reset(!sync_rst_main_n && !sh_cl_ddr_is_ready && ~&lcl_sh_cl_ddr_is_ready),
  .io_ocl_aw_ready(io_ocl_aw_ready),
  .io_ocl_aw_valid(io_ocl_aw_valid),
  .io_ocl_aw_bits_addr(io_ocl_aw_bits_addr),
  .io_ocl_aw_bits_len(io_ocl_aw_bits_len),
  .io_ocl_aw_bits_size(io_ocl_aw_bits_size),
  .io_ocl_aw_bits_burst(io_ocl_aw_bits_burst),
  .io_ocl_aw_bits_lock(io_ocl_aw_bits_lock),
  .io_ocl_aw_bits_cache(io_ocl_aw_bits_cache),
  .io_ocl_aw_bits_prot(io_ocl_aw_bits_prot),
  .io_ocl_aw_bits_qos(io_ocl_aw_bits_qos),
  .io_ocl_aw_bits_region(io_ocl_aw_bits_region),
  .io_ocl_aw_bits_id(io_ocl_aw_bits_id),
  .io_ocl_aw_bits_user(io_ocl_aw_bits_user),
  .io_ocl_w_ready(io_ocl_w_ready),
  .io_ocl_w_valid(io_ocl_w_valid),
  .io_ocl_w_bits_data(io_ocl_w_bits_data),
  .io_ocl_w_bits_last(io_ocl_w_bits_last),
  .io_ocl_w_bits_id(io_ocl_w_bits_id),
  .io_ocl_w_bits_strb(io_ocl_w_bits_strb),
  .io_ocl_w_bits_user(io_ocl_w_bits_user),
  .io_ocl_b_ready(io_ocl_b_ready),
  .io_ocl_b_valid(io_ocl_b_valid),
  .io_ocl_b_bits_resp(io_ocl_b_bits_resp),
  .io_ocl_b_bits_id(io_ocl_b_bits_id),
  .io_ocl_b_bits_user(io_ocl_b_bits_user),
  .io_ocl_ar_ready(io_ocl_ar_ready),
  .io_ocl_ar_valid(io_ocl_ar_valid),
  .io_ocl_ar_bits_addr(io_ocl_ar_bits_addr),
  .io_ocl_ar_bits_len(io_ocl_ar_bits_len),
  .io_ocl_ar_bits_size(io_ocl_ar_bits_size),
  .io_ocl_ar_bits_burst(io_ocl_ar_bits_burst),
  .io_ocl_ar_bits_lock(io_ocl_ar_bits_lock),
  .io_ocl_ar_bits_cache(io_ocl_ar_bits_cache),
  .io_ocl_ar_bits_prot(io_ocl_ar_bits_prot),
  .io_ocl_ar_bits_qos(io_ocl_ar_bits_qos),
  .io_ocl_ar_bits_region(io_ocl_ar_bits_region),
  .io_ocl_ar_bits_id(io_ocl_ar_bits_id),
  .io_ocl_ar_bits_user(io_ocl_ar_bits_user),
  .io_ocl_r_ready(io_ocl_r_ready),
  .io_ocl_r_valid(io_ocl_r_valid),
  .io_ocl_r_bits_resp(io_ocl_r_bits_resp),
  .io_ocl_r_bits_data(io_ocl_r_bits_data),
  .io_ocl_r_bits_last(io_ocl_r_bits_last),
  .io_ocl_r_bits_id(io_ocl_r_bits_id),
  .io_ocl_r_bits_user(io_ocl_r_bits_user),

  //mem
  .io_mem_0_aw_ready(io_mem_0_aw_ready),
  .io_mem_0_aw_valid(io_mem_0_aw_valid),
  .io_mem_0_aw_bits_addr(io_mem_0_aw_bits_addr),
  .io_mem_0_aw_bits_len(io_mem_0_aw_bits_len),
  .io_mem_0_aw_bits_size(io_mem_0_aw_bits_size),
  .io_mem_0_aw_bits_burst(io_mem_0_aw_bits_burst),
  .io_mem_0_aw_bits_lock(io_mem_0_aw_bits_lock),
  .io_mem_0_aw_bits_cache(io_mem_0_aw_bits_cache),
  .io_mem_0_aw_bits_prot(io_mem_0_aw_bits_prot),
  .io_mem_0_aw_bits_qos(io_mem_0_aw_bits_qos),
  .io_mem_0_aw_bits_region(io_mem_0_aw_bits_region),
  .io_mem_0_aw_bits_id(io_mem_0_aw_bits_id),
  .io_mem_0_aw_bits_user(io_mem_0_aw_bits_user),
  .io_mem_0_w_ready(io_mem_0_w_ready),
  .io_mem_0_w_valid(io_mem_0_w_valid),
  .io_mem_0_w_bits_data(io_mem_0_w_bits_data),
  .io_mem_0_w_bits_last(io_mem_0_w_bits_last),
  .io_mem_0_w_bits_id(io_mem_0_w_bits_id),
  .io_mem_0_w_bits_strb(io_mem_0_w_bits_strb),
  .io_mem_0_w_bits_user(io_mem_0_w_bits_user),
  .io_mem_0_b_ready(io_mem_0_b_ready),
  .io_mem_0_b_valid(io_mem_0_b_valid),
  .io_mem_0_b_bits_resp(io_mem_0_b_bits_resp),
  .io_mem_0_b_bits_id(io_mem_0_b_bits_id),
  .io_mem_0_b_bits_user(io_mem_0_b_bits_user),
  .io_mem_0_ar_ready(io_mem_0_ar_ready),
  .io_mem_0_ar_valid(io_mem_0_ar_valid),
  .io_mem_0_ar_bits_addr(io_mem_0_ar_bits_addr),
  .io_mem_0_ar_bits_len(io_mem_0_ar_bits_len),
  .io_mem_0_ar_bits_size(io_mem_0_ar_bits_size),
  .io_mem_0_ar_bits_burst(io_mem_0_ar_bits_burst),
  .io_mem_0_ar_bits_lock(io_mem_0_ar_bits_lock),
  .io_mem_0_ar_bits_cache(io_mem_0_ar_bits_cache),
  .io_mem_0_ar_bits_prot(io_mem_0_ar_bits_prot),
  .io_mem_0_ar_bits_qos(io_mem_0_ar_bits_qos),
  .io_mem_0_ar_bits_region(io_mem_0_ar_bits_region),
  .io_mem_0_ar_bits_id(io_mem_0_ar_bits_id),
  .io_mem_0_ar_bits_user(io_mem_0_ar_bits_user),
  .io_mem_0_r_ready(io_mem_0_r_ready),
  .io_mem_0_r_valid(io_mem_0_r_valid),
  .io_mem_0_r_bits_resp(io_mem_0_r_bits_resp),
  .io_mem_0_r_bits_data(io_mem_0_r_bits_data),
  .io_mem_0_r_bits_last(io_mem_0_r_bits_last),
  .io_mem_0_r_bits_id(io_mem_0_r_bits_id),
  .io_mem_0_r_bits_user(io_mem_0_r_bits_user)
);
   
//-------------------------------------------
// Tie-Off Global Signals
//-------------------------------------------
`ifndef CL_VERSION
   `define CL_VERSION 32'hee_ee_ee_00
`endif  

   assign cl_sh_id0[31:0]       = `CL_SH_ID0;
   assign cl_sh_id1[31:0]       = `CL_SH_ID1;
   assign cl_sh_status0[31:0]   = 32'h0000_0000;
   assign cl_sh_status1[31:0]   = `CL_VERSION;

//------------------------------------
// Tie-Off Unused AXI Interfaces
//------------------------------------

   // OCL interface to custom logic
   assign ocl_sh_awready           =   io_ocl_aw_ready;
   assign io_ocl_aw_valid          =   sh_ocl_awvalid;
   assign io_ocl_aw_bits_addr      =   sh_ocl_awaddr;
   assign io_ocl_aw_bits_len       =   8'h0;
   assign io_ocl_aw_bits_size      =   3'h2;
   assign io_ocl_aw_bits_burst     =   2'b01;
   assign io_ocl_aw_bits_lock      =   1'b0;
   assign io_ocl_aw_bits_cache     =   4'b0000;
   assign io_ocl_aw_bits_prot      =   3'b000;
   assign io_ocl_aw_bits_qos       =   4'b0000;
   assign io_ocl_aw_bits_region    =   4'b0000;
   assign io_ocl_aw_bits_id        =   10'h0;
   assign io_ocl_aw_bits_user      =   1'b0;
                                    
   assign ocl_sh_wready            =   io_ocl_w_ready;
   assign io_ocl_w_valid           =   sh_ocl_wvalid;
   assign io_ocl_w_bits_data       =   sh_ocl_wdata;
   assign io_ocl_w_bits_last       =   1'h1;
   //assign io_ocl_w_bits_id         =   sh_ocl_awid;
   assign io_ocl_w_bits_strb       =   sh_ocl_wstrb;
   assign io_ocl_w_bits_user       =   1'b0;
                                       
   assign ocl_sh_bresp             =   io_ocl_b_bits_resp;
   assign ocl_sh_bid               =   10'b0;
   assign ocl_sh_bvalid            =   io_ocl_b_valid;
   assign io_ocl_b_ready           =   sh_ocl_bready;
   assign io_ocl_b_bits_user       =   1'b0; 
                                       
   assign ocl_sh_arready           =   io_ocl_ar_ready;
   assign io_ocl_ar_valid          =   sh_ocl_arvalid;
   assign io_ocl_ar_bits_addr      =   sh_ocl_araddr;
   assign io_ocl_ar_bits_len       =   8'h0;
   assign io_ocl_ar_bits_size      =   3'h2;
   assign io_ocl_ar_bits_burst     =   2'b01;
   assign io_ocl_ar_bits_lock      =   1'b0;
   assign io_ocl_ar_bits_cache     =   4'b0000;
   assign io_ocl_ar_bits_prot      =   3'b000;
   assign io_ocl_ar_bits_qos       =   4'b0000;
   assign io_ocl_ar_bits_region    =   4'b0000;
   assign io_ocl_ar_bits_id        =   10'h0;

   assign ocl_sh_rdata             =   io_ocl_r_bits_data;
   assign ocl_sh_rresp             =   io_ocl_r_bits_resp;
   assign ocl_sh_rid               =   10'h0;
   assign ocl_sh_rlast             =   1'b0;
   assign ocl_sh_rvalid            =   io_ocl_r_valid;
   assign io_ocl_r_ready           =   sh_ocl_rready;
   assign io_ocl_r_bits_user       =   1'b0;
   
/*   // AXI-4 interface to DRAM
   // 2-way arbiter between
   // the adamacc mem channel
   // and the dma_pcis from shell
   axi_dma_arbiter ddr_arbiter (
                       .INTERCONNECT_ACLK(clk_main_a0),
                       .INTERCONNECT_ARESETN(sync_rst_main_n),
                       .S00_AXI_ARESET_OUT_N(),
                       .S00_AXI_ACLK(clk_main_a0),
                       .S00_AXI_AWID(io_mem_0_aw_bits_id_q[7:0]),
                       .S00_AXI_AWADDR(io_mem_0_aw_bits_addr_q),
                       .S00_AXI_AWLEN(io_mem_0_aw_bits_len_q),
                       .S00_AXI_AWSIZE(3'b110),
                       .S00_AXI_AWBURST(),
                       .S00_AXI_AWLOCK(),
                       .S00_AXI_AWCACHE(),
                       .S00_AXI_AWPROT(),
                       .S00_AXI_AWQOS(),
                       .S00_AXI_AWVALID(io_mem_0_aw_valid_q),
                       .S00_AXI_AWREADY(io_mem_0_aw_ready_q),
                       .S00_AXI_WDATA(io_mem_0_w_bits_data_q),
                       .S00_AXI_WSTRB(io_mem_0_w_bits_strb_q),
                       .S00_AXI_WLAST(io_mem_0_w_bits_last_q),
                       .S00_AXI_WVALID(io_mem_0_w_valid_q),
                       .S00_AXI_WREADY(io_mem_0_w_ready_q),
                       .S00_AXI_BID(io_mem_0_b_bits_id_q[7:0]),
                       .S00_AXI_BRESP(io_mem_0_b_bits_resp_q),
                       .S00_AXI_BVALID(io_mem_0_b_valid_q),
                       .S00_AXI_BREADY(io_mem_0_b_ready_q),
                       .S00_AXI_ARID(io_mem_0_ar_bits_id_q[7:0]),
                       .S00_AXI_ARADDR(io_mem_0_ar_bits_addr_q),
                       .S00_AXI_ARLEN(io_mem_0_ar_bits_len_q),
                       .S00_AXI_ARSIZE(3'b110),
                       .S00_AXI_ARBURST(),
                       .S00_AXI_ARLOCK(),
                       .S00_AXI_ARCACHE(),
                       .S00_AXI_ARPROT(),
                       .S00_AXI_ARQOS(),
                       .S00_AXI_ARVALID(io_mem_0_ar_valid_q),
                       .S00_AXI_ARREADY(io_mem_0_ar_ready_q),
                       .S00_AXI_RID(io_mem_0_r_bits_id_q[7:0]),
                       .S00_AXI_RDATA(io_mem_0_r_bits_data_q),
                       .S00_AXI_RRESP(io_mem_0_r_bits_resp_q),
                       .S00_AXI_RLAST(io_mem_0_r_bits_last_q),
                       .S00_AXI_RVALID(io_mem_0_r_valid_q),
                       .S00_AXI_RREADY(io_mem_0_r_ready_q),
                       .S01_AXI_ARESET_OUT_N(),
                       .S01_AXI_ACLK(clk_main_a0),
                       .S01_AXI_AWID(sh_cl_dma_pcis_awid),
                       .S01_AXI_AWADDR(sh_cl_dma_pcis_awaddr),
                       .S01_AXI_AWLEN(sh_cl_dma_pcis_awlen),
                       .S01_AXI_AWSIZE(sh_cl_dma_pcis_awsize),
                       .S01_AXI_AWBURST(),
                       .S01_AXI_AWLOCK(),
                       .S01_AXI_AWCACHE(),
                       .S01_AXI_AWPROT(),
                       .S01_AXI_AWQOS(),
                       .S01_AXI_AWVALID(sh_cl_dma_pcis_awvalid),
                       .S01_AXI_AWREADY(cl_sh_dma_pcis_awready),
                       .S01_AXI_WDATA(sh_cl_dma_pcis_wdata),
                       .S01_AXI_WSTRB(sh_cl_dma_pcis_wstrb),
                       .S01_AXI_WLAST(sh_cl_dma_pcis_wlast),
                       .S01_AXI_WVALID(sh_cl_dma_pcis_wvalid),
                       .S01_AXI_WREADY(cl_sh_dma_pcis_wready),
                       .S01_AXI_BID(cl_sh_dma_pcis_bid),
                       .S01_AXI_BRESP(cl_sh_dma_pcis_bresp),
                       .S01_AXI_BVALID(cl_sh_dma_pcis_bvalid),
                       .S01_AXI_BREADY(sh_cl_dma_pcis_bready),
                       .S01_AXI_ARID(sh_cl_dma_pcis_arid),
                       .S01_AXI_ARADDR(sh_cl_dma_pcis_araddr),
                       .S01_AXI_ARLEN(sh_cl_dma_pcis_arlen),
                       .S01_AXI_ARSIZE(sh_cl_dma_pcis_arsize),
                       .S01_AXI_ARBURST(),
                       .S01_AXI_ARLOCK(),
                       .S01_AXI_ARCACHE(),
                       .S01_AXI_ARPROT(),
                       .S01_AXI_ARQOS(),
                       .S01_AXI_ARVALID(sh_cl_dma_pcis_arvalid),
                       .S01_AXI_ARREADY(cl_sh_dma_pcis_arready),
                       .S01_AXI_RID(cl_sh_dma_pcis_rid),
                       .S01_AXI_RDATA(cl_sh_dma_pcis_rdata),
                       .S01_AXI_RRESP(cl_sh_dma_pcis_rresp),
                       .S01_AXI_RLAST(cl_sh_dma_pcis_rlast),
                       .S01_AXI_RVALID(cl_sh_dma_pcis_rvalid),
                       .S01_AXI_RREADY(sh_cl_dma_pcis_rready),
                       .M00_AXI_ARESET_OUT_N(),
                       .M00_AXI_ACLK(clk_main_a0),
                       .M00_AXI_AWID(cl_sh_ddr_awid[11:0]),
                       .M00_AXI_AWADDR(cl_sh_ddr_awaddr),
                       .M00_AXI_AWLEN(cl_sh_ddr_awlen),
                       .M00_AXI_AWSIZE(cl_sh_ddr_awsize),
                       .M00_AXI_AWBURST(),
                       .M00_AXI_AWLOCK(),
                       .M00_AXI_AWCACHE(),
                       .M00_AXI_AWPROT(),
                       .M00_AXI_AWQOS(),
                       .M00_AXI_AWVALID(cl_sh_ddr_awvalid),
                       .M00_AXI_AWREADY(sh_cl_ddr_awready),
                       .M00_AXI_WDATA(cl_sh_ddr_wdata),
                       .M00_AXI_WSTRB(cl_sh_ddr_wstrb),
                       .M00_AXI_WLAST(cl_sh_ddr_wlast),
                       .M00_AXI_WVALID(cl_sh_ddr_wvalid),
                       .M00_AXI_WREADY(sh_cl_ddr_wready),
                       .M00_AXI_BID(sh_cl_ddr_bid[11:0]),
                       .M00_AXI_BRESP(sh_cl_ddr_bresp),
                       .M00_AXI_BVALID(sh_cl_ddr_bvalid),
                       .M00_AXI_BREADY(cl_sh_ddr_bready),
                       .M00_AXI_ARID(cl_sh_ddr_arid[11:0]),
                       .M00_AXI_ARADDR(cl_sh_ddr_araddr),
                       .M00_AXI_ARLEN(cl_sh_ddr_arlen),
                       .M00_AXI_ARSIZE(cl_sh_ddr_arsize),
                       .M00_AXI_ARBURST(),
                       .M00_AXI_ARLOCK(),
                       .M00_AXI_ARCACHE(),
                       .M00_AXI_ARPROT(),
                       .M00_AXI_ARQOS(),
                       .M00_AXI_ARVALID(cl_sh_ddr_arvalid),
                       .M00_AXI_ARREADY(sh_cl_ddr_arready),
                       .M00_AXI_RID(sh_cl_ddr_rid[11:0]),
                       .M00_AXI_RDATA(sh_cl_ddr_rdata),
                       .M00_AXI_RRESP(sh_cl_ddr_rresp),
                       .M00_AXI_RLAST(sh_cl_ddr_rlast),
                       .M00_AXI_RVALID(sh_cl_ddr_rvalid),
                       .M00_AXI_RREADY(cl_sh_ddr_rready)
                       );    
*/

axi_crossbar_1 axi_crossbar_1 (
  .aclk(clk_main_a0),                      // input wire aclk
  .aresetn(sync_rst_main_n),                // input wire aresetn
  .s_axi_awid({io_mem_0_aw_bits_id_q, sh_cl_dma_pcis_awid}),          // input wire [31 : 0] s_axi_awid
  .s_axi_awaddr({io_mem_0_aw_bits_addr_q, sh_cl_dma_pcis_awaddr}),      // input wire [127 : 0] s_axi_awaddr
  .s_axi_awlen({io_mem_0_aw_bits_len_q, sh_cl_dma_pcis_awlen}),        // input wire [15 : 0] s_axi_awlen
  .s_axi_awsize({3'b110, sh_cl_dma_pcis_awsize}),      // input wire [5 : 0] s_axi_awsize
  .s_axi_awburst(),    // input wire [3 : 0] s_axi_awburst
  .s_axi_awlock(),      // input wire [1 : 0] s_axi_awlock
  .s_axi_awcache(),    // input wire [7 : 0] s_axi_awcache
  .s_axi_awprot(),      // input wire [5 : 0] s_axi_awprot
  .s_axi_awqos(),        // input wire [7 : 0] s_axi_awqos
  .s_axi_awvalid({io_mem_0_aw_valid_q, sh_cl_dma_pcis_awvalid}),    // input wire [1 : 0] s_axi_awvalid
  .s_axi_awready({io_mem_0_aw_ready_q, cl_sh_dma_pcis_awready}),    // output wire [1 : 0] s_axi_awready
  .s_axi_wdata({io_mem_0_w_bits_data_q, sh_cl_dma_pcis_wdata}),        // input wire [1023 : 0] s_axi_wdata
  .s_axi_wstrb({io_mem_0_w_bits_strb_q, sh_cl_dma_pcis_wstrb}),        // input wire [127 : 0] s_axi_wstrb
  .s_axi_wlast({io_mem_0_w_bits_last_q, sh_cl_dma_pcis_wlast}),        // input wire [1 : 0] s_axi_wlast
  .s_axi_wvalid({io_mem_0_w_valid_q, sh_cl_dma_pcis_wvalid}),      // input wire [1 : 0] s_axi_wvalid
  .s_axi_wready({io_mem_0_w_ready_q, cl_sh_dma_pcis_wready}),      // output wire [1 : 0] s_axi_wready
  .s_axi_bid({io_mem_0_b_bits_id_q, cl_sh_dma_pcis_bid}),            // output wire [31 : 0] s_axi_bid
  .s_axi_bresp({io_mem_0_b_bits_resp_q, cl_sh_dma_pcis_bresp}),        // output wire [3 : 0] s_axi_bresp
  .s_axi_bvalid({io_mem_0_b_valid_q, cl_sh_dma_pcis_bvalid}),      // output wire [1 : 0] s_axi_bvalid
  .s_axi_bready({io_mem_0_b_ready_q, sh_cl_dma_pcis_bready}),      // input wire [1 : 0] s_axi_bready
  .s_axi_arid({io_mem_0_ar_bits_id_q, sh_cl_dma_pcis_arid}),          // input wire [31 : 0] s_axi_arid
  .s_axi_araddr({io_mem_0_ar_bits_addr_q, sh_cl_dma_pcis_araddr}),      // input wire [127 : 0] s_axi_araddr
  .s_axi_arlen({io_mem_0_ar_bits_len_q, sh_cl_dma_pcis_arlen}),        // input wire [15 : 0] s_axi_arlen
  .s_axi_arsize({3'b110, sh_cl_dma_pcis_arsize}),      // input wire [5 : 0] s_axi_arsize
  .s_axi_arburst(),    // input wire [3 : 0] s_axi_arburst
  .s_axi_arlock(),      // input wire [1 : 0] s_axi_arlock
  .s_axi_arcache(),    // input wire [7 : 0] s_axi_arcache
  .s_axi_arprot(),      // input wire [5 : 0] s_axi_arprot
  .s_axi_arqos(),        // input wire [7 : 0] s_axi_arqos
  .s_axi_arvalid({io_mem_0_ar_valid_q, sh_cl_dma_pcis_arvalid}),    // input wire [1 : 0] s_axi_arvalid
  .s_axi_arready({io_mem_0_ar_ready_q, cl_sh_dma_pcis_arready}),    // output wire [1 : 0] s_axi_arready
  .s_axi_rid({io_mem_0_r_bits_id_q, cl_sh_dma_pcis_rid}),            // output wire [31 : 0] s_axi_rid
  .s_axi_rdata({io_mem_0_r_bits_data_q, cl_sh_dma_pcis_rdata}),        // output wire [1023 : 0] s_axi_rdata
  .s_axi_rresp({io_mem_0_r_bits_resp_q, cl_sh_dma_pcis_rresp}),        // output wire [3 : 0] s_axi_rresp
  .s_axi_rlast({io_mem_0_r_bits_last_q, cl_sh_dma_pcis_rlast}),        // output wire [1 : 0] s_axi_rlast
  .s_axi_rvalid({io_mem_0_r_valid_q, cl_sh_dma_pcis_rvalid}),      // output wire [1 : 0] s_axi_rvalid
  .s_axi_rready({io_mem_0_r_ready_q, sh_cl_dma_pcis_rready}),      // input wire [1 : 0] s_axi_rready
  .m_axi_awid(cl_sh_ddr_awid),          // output wire [15 : 0] m_axi_awid
  .m_axi_awaddr(cl_sh_ddr_awaddr),      // output wire [63 : 0] m_axi_awaddr
  .m_axi_awlen(cl_sh_ddr_awlen),        // output wire [7 : 0] m_axi_awlen
  .m_axi_awsize(cl_sh_ddr_awsize),      // output wire [2 : 0] m_axi_awsize
  .m_axi_awburst(),    // output wire [1 : 0] m_axi_awburst
  .m_axi_awlock(),      // output wire [0 : 0] m_axi_awlock
  .m_axi_awcache(),    // output wire [3 : 0] m_axi_awcache
  .m_axi_awprot(),      // output wire [2 : 0] m_axi_awprot
  .m_axi_awregion(),  // output wire [3 : 0] m_axi_awregion
  .m_axi_awqos(),        // output wire [3 : 0] m_axi_awqos
  .m_axi_awvalid(cl_sh_ddr_awvalid),    // output wire [0 : 0] m_axi_awvalid
  .m_axi_awready(sh_cl_ddr_awready),    // input wire [0 : 0] m_axi_awready
  .m_axi_wdata(cl_sh_ddr_wdata),        // output wire [511 : 0] m_axi_wdata
  .m_axi_wstrb(cl_sh_ddr_wstrb),        // output wire [63 : 0] m_axi_wstrb
  .m_axi_wlast(cl_sh_ddr_wlast),        // output wire [0 : 0] m_axi_wlast
  .m_axi_wvalid(cl_sh_ddr_wvalid),      // output wire [0 : 0] m_axi_wvalid
  .m_axi_wready(sh_cl_ddr_wready),      // input wire [0 : 0] m_axi_wready
  .m_axi_bid(sh_cl_ddr_bid),            // input wire [15 : 0] m_axi_bid
  .m_axi_bresp(sh_cl_ddr_bresp),        // input wire [1 : 0] m_axi_bresp
  .m_axi_bvalid(sh_cl_ddr_bvalid),      // input wire [0 : 0] m_axi_bvalid
  .m_axi_bready(cl_sh_ddr_bready),      // output wire [0 : 0] m_axi_bready
  .m_axi_arid(cl_sh_ddr_arid),          // output wire [15 : 0] m_axi_arid
  .m_axi_araddr(cl_sh_ddr_araddr),      // output wire [63 : 0] m_axi_araddr
  .m_axi_arlen(cl_sh_ddr_arlen),        // output wire [7 : 0] m_axi_arlen
  .m_axi_arsize(cl_sh_ddr_arsize),      // output wire [2 : 0] m_axi_arsize
  .m_axi_arburst(),    // output wire [1 : 0] m_axi_arburst
  .m_axi_arlock(),      // output wire [0 : 0] m_axi_arlock
  .m_axi_arcache(),    // output wire [3 : 0] m_axi_arcache
  .m_axi_arprot(),      // output wire [2 : 0] m_axi_arprot
  .m_axi_arregion(),  // output wire [3 : 0] m_axi_arregion
  .m_axi_arqos(),        // output wire [3 : 0] m_axi_arqos
  .m_axi_arvalid(cl_sh_ddr_arvalid),    // output wire [0 : 0] m_axi_arvalid
  .m_axi_arready(sh_cl_ddr_arready),    // input wire [0 : 0] m_axi_arready
  .m_axi_rid(sh_cl_ddr_rid),            // input wire [15 : 0] m_axi_rid
  .m_axi_rdata(sh_cl_ddr_rdata),        // input wire [511 : 0] m_axi_rdata
  .m_axi_rresp(sh_cl_ddr_rresp),        // input wire [1 : 0] m_axi_rresp
  .m_axi_rlast(sh_cl_ddr_rlast),        // input wire [0 : 0] m_axi_rlast
  .m_axi_rvalid(sh_cl_ddr_rvalid),      // input wire [0 : 0] m_axi_rvalid
  .m_axi_rready(cl_sh_ddr_rready)      // output wire [0 : 0] m_axi_rready
);

endmodule
