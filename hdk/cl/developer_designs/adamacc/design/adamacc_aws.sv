// Amazon FPGA Hardware Development Kit
//
// Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
//
// Licensed under the Amazon Software License (the "License"). You may not use
// this file except in compliance with the License. A copy of the License is
// located at
//
//    http://aws.amazon.com/asl/
//
// or in the "license" file accompanying this file. This file is distributed on
// an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or
// implied. See the License for the specific language governing permissions and
// limitations under the License.
`include "adamacc_defines.vh"
`include "cl_id_defines.vh"

module adamacc_aws #(parameter NUM_PCIE=1, parameter NUM_DDR=4, parameter NUM_HMC=4, parameter NUM_GTY = 4) 

   (
`include "cl_ports.vh" // Fixed port definition

    );
   
   localparam NUM_CFG_STGS_INT_TST = 4;
   localparam NUM_CFG_STGS_HMC_ATG = 4;
   localparam NUM_CFG_STGS_CL_DDR_ATG = 4;
   localparam NUM_CFG_STGS_SH_DDR_ATG = 4;
   localparam NUM_CFG_STGS_PCIE_ATG = 4;
   localparam NUM_CFG_STGS_AURORA_ATG = 4;
   localparam NUM_CFG_STGS_XDCFG = 4;
   localparam NUM_CFG_STGS_XDMA = 4;

`include "unused_ddr_a_b_d_template.inc"
`include "unused_ddr_c_template.inc"
`include "unused_pcim_template.inc"
`include "unused_dma_pcis_template.inc"
`include "unused_flr_template.inc"
`include "unused_apppf_irq_template.inc"
`include "unused_cl_sda_template.inc"
`include "unused_sh_bar1_template.inc"
   
   //-------------------------------------------------
   // Reset Synchronization
   //-------------------------------------------------
   logic pre_sync_rst_main_n;
   logic sync_rst_main_n;
   logic pre_sync_rst_n_extra;
   logic sync_rst_extra_n;
   
   always_ff @(negedge rst_main_n or posedge clk_main_a0)
     if (!rst_main_n)
       begin
          pre_sync_rst_main_n <= 0;
          sync_rst_main_n <= 0;
       end
     else
       begin
          pre_sync_rst_main_n <= 1;
          sync_rst_main_n <= pre_sync_rst_main_n;
       end

   always_ff @(negedge rst_main_n or posedge clk_extra_a1)
     if (!rst_main_n)
       begin
          pre_sync_rst_n_extra  <= 0;
          sync_rst_extra_n <= 0;
       end
     else
       begin
          pre_sync_rst_n_extra  <= 1;
          sync_rst_extra_n <= pre_sync_rst_n_extra;
       end
   
   //-------------------------------------------
   // Wires for generated verilog
   //-------------------------------------------

   //PCI ocl AXI
   wire          io_ocl_aw_ready;
   wire          io_ocl_aw_valid;
   wire [31:0]   io_ocl_aw_bits_addr;
   wire [7:0]    io_ocl_aw_bits_len;
   wire [2:0]    io_ocl_aw_bits_size;
   wire [1:0]    io_ocl_aw_bits_burst;
   wire          io_ocl_aw_bits_lock;
   wire [3:0]    io_ocl_aw_bits_cache;
   wire [2:0]    io_ocl_aw_bits_prot;
   wire [3:0]    io_ocl_aw_bits_qos;
   wire [3:0]    io_ocl_aw_bits_region;
   wire [9:0]    io_ocl_aw_bits_id;
   wire          io_ocl_aw_bits_user;
   wire          io_ocl_w_ready;
   wire          io_ocl_w_valid;
   wire [31:0]   io_ocl_w_bits_data;
   wire          io_ocl_w_bits_last;
   wire [9:0]    io_ocl_w_bits_id;
   wire [3:0]    io_ocl_w_bits_strb;
   wire          io_ocl_w_bits_user;
   wire          io_ocl_b_ready;
   wire          io_ocl_b_valid;
   wire [1:0]    io_ocl_b_bits_resp;
   wire [9:0]    io_ocl_b_bits_id;
   wire          io_ocl_b_bits_user;
   wire          io_ocl_ar_ready;
   wire          io_ocl_ar_valid;
   wire [31:0]   io_ocl_ar_bits_addr;
   wire [7:0]    io_ocl_ar_bits_len;
   wire [2:0]    io_ocl_ar_bits_size;
   wire [1:0]    io_ocl_ar_bits_burst;
   wire          io_ocl_ar_bits_lock;
   wire [3:0]    io_ocl_ar_bits_cache;
   wire [2:0]    io_ocl_ar_bits_prot;
   wire [3:0]    io_ocl_ar_bits_qos;
   wire [3:0]    io_ocl_ar_bits_region;
   wire [9:0]    io_ocl_ar_bits_id;
   wire          io_ocl_ar_bits_user;
   wire          io_ocl_r_ready;
   wire          io_ocl_r_valid;
   wire [1:0]    io_ocl_r_bits_resp;
   wire [31:0]   io_ocl_r_bits_data;
   wire          io_ocl_r_bits_last;
   wire [9:0]    io_ocl_r_bits_id;
   wire          io_ocl_r_bits_user;


   //-------------------------------------------
   // Instatiate generated verilog
   //-------------------------------------------

   TestHarness generated_top (
                              .clock(clk_main_a0),
                              .reset(!sync_rst_main_n),
                              .io_ocl_0_aw_ready(io_ocl_aw_ready),
                              .io_ocl_0_aw_valid(io_ocl_aw_valid),
                              .io_ocl_0_aw_bits_id(io_ocl_aw_bits_id),
                              .io_ocl_0_aw_bits_addr(io_ocl_aw_bits_addr),
                              .io_ocl_0_aw_bits_len(io_ocl_aw_bits_len),
                              .io_ocl_0_aw_bits_size(io_ocl_aw_bits_size),
                              .io_ocl_0_aw_bits_burst(io_ocl_aw_bits_burst),
                              .io_ocl_0_aw_bits_lock(io_ocl_aw_bits_lock),
                              .io_ocl_0_aw_bits_cache(io_ocl_aw_bits_cache),
                              .io_ocl_0_aw_bits_prot(io_ocl_aw_bits_prot),
                              .io_ocl_0_aw_bits_qos(io_ocl_aw_bits_qos),
                              .io_ocl_0_w_ready(io_ocl_w_ready),
                              .io_ocl_0_w_valid(io_ocl_w_valid),
                              .io_ocl_0_w_bits_data(io_ocl_w_bits_data),
                              .io_ocl_0_w_bits_strb(io_ocl_w_bits_strb),
                              .io_ocl_0_w_bits_last(io_ocl_w_bits_last),
                              .io_ocl_0_b_ready(io_ocl_b_ready),
                              .io_ocl_0_b_valid(io_ocl_b_valid),
                              .io_ocl_0_b_bits_id(io_ocl_b_bits_id),
                              .io_ocl_0_b_bits_resp(io_ocl_b_bits_resp),
                              .io_ocl_0_ar_ready(io_ocl_ar_ready),
                              .io_ocl_0_ar_valid(io_ocl_ar_valid),
                              .io_ocl_0_ar_bits_id(io_ocl_ar_bits_id),
                              .io_ocl_0_ar_bits_addr(io_ocl_ar_bits_addr),
                              .io_ocl_0_ar_bits_len(io_ocl_ar_bits_len),
                              .io_ocl_0_ar_bits_size(io_ocl_ar_bits_size),
                              .io_ocl_0_ar_bits_burst(io_ocl_ar_bits_burst),
                              .io_ocl_0_ar_bits_lock(io_ocl_ar_bits_lock),
                              .io_ocl_0_ar_bits_cache(io_ocl_ar_bits_cache),
                              .io_ocl_0_ar_bits_prot(io_ocl_ar_bits_prot),
                              .io_ocl_0_ar_bits_qos(io_ocl_ar_bits_qos),
                              .io_ocl_0_r_ready(io_ocl_r_ready),
                              .io_ocl_0_r_valid(io_ocl_r_valid),
                              .io_ocl_0_r_bits_id(io_ocl_r_bits_id),
                              .io_ocl_0_r_bits_data(io_ocl_r_bits_data),
                              .io_ocl_0_r_bits_resp(io_ocl_r_bits_resp),
                              .io_ocl_0_r_bits_last(io_ocl_r_bits_last)
                              );
   //-------------------------------------------
   // Tie-Off Global Signals
   //-------------------------------------------
`ifndef CL_VERSION
 `define CL_VERSION 32'hee_ee_ee_00
`endif  

   assign cl_sh_id0[31:0]       = `CL_SH_ID0;
   assign cl_sh_id1[31:0]       = `CL_SH_ID1;
   assign cl_sh_status0[31:0]   = 32'h0000_0000;
   assign cl_sh_status1[31:0]   = `CL_VERSION;

   //------------------------------------
   // Tie-Off Unused AXI Interfaces
   //------------------------------------

   // PCIe Interface from SH to CL
   assign ocl_sh_awready   =   io_ocl_aw_ready;
   assign io_ocl_aw_valid          =   sh_ocl_awvalid;
   assign io_ocl_aw_bits_addr      =   sh_ocl_awaddr;
   assign io_ocl_aw_bits_len       =   8'h0;
   assign io_ocl_aw_bits_size      =   3'h2;
   assign io_ocl_aw_bits_burst     =   2'b01;
   assign io_ocl_aw_bits_lock      =   1'b0;
   assign io_ocl_aw_bits_cache     =   4'b0000;
   assign io_ocl_aw_bits_prot      =   3'b000;
   assign io_ocl_aw_bits_qos       =   4'b0000;
   assign io_ocl_aw_bits_region    =   4'b0000;
   assign io_ocl_aw_bits_id        =   10'h0;
   assign io_ocl_aw_bits_user      =   1'b0;

   assign ocl_sh_wready    =   io_ocl_w_ready;
   assign io_ocl_w_valid           =   sh_ocl_wvalid;
   assign io_ocl_w_bits_data       =   sh_ocl_wdata;
   assign io_ocl_w_bits_last       =   1'h1;
   //assign io_ocl_w_bits_id         =   sh_ocl_awid;
   assign io_ocl_w_bits_strb       =   sh_ocl_wstrb;
   assign io_ocl_w_bits_user       =   1'b0;   

   assign ocl_sh_bresp     =   io_ocl_b_bits_resp;
   assign ocl_sh_bid       =   10'b0;
   assign ocl_sh_bvalid    =   io_ocl_b_valid;
   assign io_ocl_b_ready           =   sh_ocl_bready;
   assign io_ocl_b_bits_user       =   1'b0; 

   assign ocl_sh_arready   =   io_ocl_ar_ready;
   assign io_ocl_ar_valid          =   sh_ocl_arvalid;
   assign io_ocl_ar_bits_addr      =   sh_ocl_araddr;
   assign io_ocl_ar_bits_len       =   8'h0;
   assign io_ocl_ar_bits_size      =   3'h2;
   assign io_ocl_ar_bits_burst     =   2'b01;
   assign io_ocl_ar_bits_lock      =   1'b0;
   assign io_ocl_ar_bits_cache     =   4'b0000;
   assign io_ocl_ar_bits_prot      =   3'b000;
   assign io_ocl_ar_bits_qos       =   4'b0000;
   assign io_ocl_ar_bits_region    =   4'b0000;
   assign io_ocl_ar_bits_id        =   10'h0;

   assign ocl_sh_rdata     =   io_ocl_r_bits_data;
   assign ocl_sh_rresp     =   io_ocl_r_bits_resp;
   assign ocl_sh_rid       =   10'h0;
   assign ocl_sh_rlast     =   1'b0;
   assign ocl_sh_rvalid    =   io_ocl_r_valid;
   assign io_ocl_r_ready           =   sh_ocl_rready;
   assign io_ocl_r_bits_user       =   1'b0;
            

endmodule
