#!/usr/bin/bash

if [ "$#" -ne 1 ]; then
    echo "ERROR! Illegal number of parameters!"
    echo "usage: ./load-permission.sh <image id afi-*>"
    exit
fi

MYAFI=$1
# this is giving Brendan permission to load the specified AFI using AGFI id
aws ec2 --region us-east-1 modify-fpga-image-attribute --fpga-image-id ${MYAFI} --operation-type add --user-ids 505379851668
# this is giving David permission to load the specified AFI using AGFI id
aws ec2 --region us-east-1 modify-fpga-image-attribute --fpga-image-id ${MYAFI} --operation-type add --user-ids 440590964962
# this is giving Lisa permission to load the specific AFI using AGFI id
aws ec2 --region us-east-1 modify-fpga-image-attribute --fpga-image-id ${MYAFI} --operation-type add --user-ids 243220685629 
