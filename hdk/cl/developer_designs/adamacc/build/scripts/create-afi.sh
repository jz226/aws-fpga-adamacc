#!/usr/bin/bash

if [ "$#" -ne 2 ]; then
    echo "ERROR! Illegal number of parameters!"
    echo "usage: ./create-afi.sh <tarball name *.Develoepr_CL.tar> <easy to understand name>"
    exit
fi

MYTARBALL=$1
MYNAME=$2
aws ec2 create-fpga-image --name ${MYNAME} --description ${MYNAME} --input-storage-location Bucket=adamacc/build-tarballs,Key=${MYTARBALL} --logs-storage-location Bucket=adamacc,Key=logfiles
