#!/usr/bin/bash

if [ "$#" -ne 1 ]; then
    echo "ERROR! Illegal number of parameters!"
    echo "usage: ./cp-to-s3.sh <tarball name in *.Developer_CL.tar>"
    exit
fi
MYTARBALL=$1
aws s3 cp ${CL_DIR}/build/checkpoints/to_aws/${MYTARBALL} s3://adamacc/build-tarballs/ --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers
