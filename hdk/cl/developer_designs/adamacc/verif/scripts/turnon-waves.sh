#!/bin/bash
MAKEDIR=${CL_DIR}/verif/scripts
TBDIR=${CL_DIR}/../../../common/verif/tb/sv

if [ -f ${MAKEDIR}/Makefile.orig ]; then
    echo "${MAKEDIR}/Makefile.vcs already modified."
else    
    mv ${MAKEDIR}/Makefile.vcs ${MAKEDIR}/Makefile.orig
    mv ${MAKEDIR}/Makefile.waves ${MAKEDIR}/Makefile.vcs
fi

if [ -f ${TBDIR}/tb.sv.orig ]; then
    echo "${TBDIR}/tb.sv already modified."
else    
    mv ${TBDIR}/tb.sv ${TBDIR}/tb.sv.orig
    mv ${TBDIR}/tb.sv.waves ${TBDIR}/tb.sv
fi
