#!/bin/bash
MAKEDIR=${CL_DIR}/verif/scripts
TBDIR=${CL_DIR}/../../../common/verif/tb/sv

if [ -f ${MAKEDIR}/Makefile.waves ]; then
    echo "${MAKEDIR}/Makefile.vcs already restored."
else    
    mv ${MAKEDIR}/Makefile.vcs ${MAKEDIR}/Makefile.waves
    mv ${MAKEDIR}/Makefile.orig ${MAKEDIR}/Makefile.vcs
fi

if [ -f ${TBDIR}/tb.sv.waves ]; then
    echo "${TBDIR}/tb.sv already restored."
else    
    mv ${TBDIR}/tb.sv ${TBDIR}/tb.sv.waves
    mv ${TBDIR}/tb.sv.orig ${TBDIR}/tb.sv
fi
